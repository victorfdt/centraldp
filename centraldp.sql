-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: centraldp
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configurations`
--

DROP TABLE IF EXISTS `configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configurations` (
  `max_num_ins` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configurations`
--

LOCK TABLES `configurations` WRITE;
/*!40000 ALTER TABLE `configurations` DISABLE KEYS */;
INSERT INTO `configurations` VALUES (5,NULL,NULL);
/*!40000 ALTER TABLE `configurations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course_subject`
--

DROP TABLE IF EXISTS `course_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_subject` (
  `course_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `course_subject_course_id_index` (`course_id`),
  KEY `course_subject_subject_id_index` (`subject_id`),
  CONSTRAINT `course_subject_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `course_subject_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course_subject`
--

LOCK TABLES `course_subject` WRITE;
/*!40000 ALTER TABLE `course_subject` DISABLE KEYS */;
INSERT INTO `course_subject` VALUES (10,13,'2016-11-24 00:59:10','2016-11-24 00:59:10'),(7,13,'2016-11-24 00:59:10','2016-11-24 00:59:10'),(8,13,'2016-11-24 00:59:10','2016-11-24 00:59:10'),(6,13,'2016-11-24 00:59:10','2016-11-24 00:59:10'),(5,13,'2016-11-24 00:59:10','2016-11-24 00:59:10'),(3,13,'2016-11-24 00:59:10','2016-11-24 00:59:10'),(4,13,'2016-11-24 00:59:10','2016-11-24 00:59:10'),(3,3,'2016-11-24 00:59:34','2016-11-24 00:59:34'),(4,3,'2016-11-24 00:59:34','2016-11-24 00:59:34'),(10,4,'2016-11-24 00:59:41','2016-11-24 00:59:41'),(8,4,'2016-11-24 00:59:41','2016-11-24 00:59:41'),(7,4,'2016-11-24 00:59:41','2016-11-24 00:59:41'),(6,4,'2016-11-24 00:59:41','2016-11-24 00:59:41'),(4,4,'2016-11-24 00:59:41','2016-11-24 00:59:41'),(3,4,'2016-11-24 00:59:41','2016-11-24 00:59:41'),(5,12,'2016-11-24 01:00:22','2016-11-24 01:00:22'),(10,5,'2016-11-24 01:00:34','2016-11-24 01:00:34'),(7,5,'2016-11-24 01:00:34','2016-11-24 01:00:34'),(8,5,'2016-11-24 01:00:34','2016-11-24 01:00:34'),(6,5,'2016-11-24 01:00:34','2016-11-24 01:00:34'),(10,6,'2016-11-24 01:00:47','2016-11-24 01:00:47'),(8,6,'2016-11-24 01:00:47','2016-11-24 01:00:47'),(7,6,'2016-11-24 01:00:47','2016-11-24 01:00:47'),(6,6,'2016-11-24 01:00:47','2016-11-24 01:00:47'),(9,8,'2016-11-24 01:03:06','2016-11-24 01:03:06'),(5,15,'2016-11-24 01:03:22','2016-11-24 01:03:22'),(5,14,'2016-11-24 01:03:32','2016-11-24 01:03:32'),(9,11,'2016-11-24 01:04:11','2016-11-24 01:04:11'),(9,7,'2016-11-24 01:04:20','2016-11-24 01:04:20'),(9,10,'2016-11-24 01:04:31','2016-11-24 01:04:31'),(9,9,'2016-11-24 01:04:45','2016-11-24 01:04:45');
/*!40000 ALTER TABLE `course_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `period` int(11) NOT NULL,
  `institution_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_institution_id_foreign` (`institution_id`),
  CONSTRAINT `courses_institution_id_foreign` FOREIGN KEY (`institution_id`) REFERENCES `institutions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (3,'Odontologia',1,1,'2016-11-10 23:16:14','2016-11-10 23:16:14'),(4,'Odontologia',2,1,'2016-11-10 23:16:36','2016-11-10 23:16:36'),(5,'Jornalismo',2,1,'2016-11-10 23:16:50','2016-11-10 23:16:50'),(6,'Engenharia de Produção',2,1,'2016-11-10 23:17:03','2016-11-10 23:17:03'),(7,'Engenharia de Computação',2,1,'2016-11-10 23:17:21','2016-11-10 23:17:21'),(8,'Engenharia de Computação',1,1,'2016-11-10 23:17:33','2016-11-10 23:17:33'),(9,'Direito',2,1,'2016-11-10 23:17:54','2016-11-10 23:17:54'),(10,'Engenharia Civil',2,1,'2016-11-10 23:18:10','2016-11-10 23:18:10');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dpclass_user`
--

DROP TABLE IF EXISTS `dpclass_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dpclass_user` (
  `dpclass_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `dpclass_user_dpclass_id_index` (`dpclass_id`),
  KEY `dpclass_user_user_id_index` (`user_id`),
  CONSTRAINT `dpclass_user_dpclass_id_foreign` FOREIGN KEY (`dpclass_id`) REFERENCES `dpclasses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `dpclass_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dpclass_user`
--

LOCK TABLES `dpclass_user` WRITE;
/*!40000 ALTER TABLE `dpclass_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `dpclass_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dpclasses`
--

DROP TABLE IF EXISTS `dpclasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dpclasses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `professor_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `professor_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `days` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dpclasses_subject_id_index` (`subject_id`),
  CONSTRAINT `dpclasses_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dpclasses`
--

LOCK TABLES `dpclasses` WRITE;
/*!40000 ALTER TABLE `dpclasses` DISABLE KEYS */;
/*!40000 ALTER TABLE `dpclasses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscriptions`
--

DROP TABLE IF EXISTS `inscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `days` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inscriptions_user_id_index` (`user_id`),
  KEY `inscriptions_subject_id_index` (`subject_id`),
  CONSTRAINT `inscriptions_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `inscriptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscriptions`
--

LOCK TABLES `inscriptions` WRITE;
/*!40000 ALTER TABLE `inscriptions` DISABLE KEYS */;
INSERT INTO `inscriptions` VALUES (22,1,'MonWedThu',1,11,6,'2016-11-24 01:16:34','2016-11-24 01:16:34'),(23,1,'ThuSat',2,11,13,'2016-11-24 01:16:53','2016-11-24 01:16:53'),(24,1,'FriSat',3,11,4,'2016-11-24 01:17:07','2016-11-24 01:17:07'),(25,1,'ThuFri',1,12,13,'2016-11-24 01:17:52','2016-11-24 01:17:52'),(26,1,'MonThu',2,12,4,'2016-11-24 01:18:19','2016-11-24 01:18:19'),(27,1,'TueFri',3,12,3,'2016-11-24 01:18:40','2016-11-24 01:18:40'),(28,1,'ThuFri',1,13,13,'2016-11-24 01:19:36','2016-11-24 01:19:36'),(29,1,'MonTue',2,13,3,'2016-11-24 01:22:17','2016-11-24 01:22:17'),(30,1,'MonThuFri',1,14,6,'2016-11-24 01:22:55','2016-11-24 01:22:55'),(31,1,'TueFri',2,14,5,'2016-11-24 01:23:04','2016-11-24 01:23:04'),(32,1,'WedThu',3,14,4,'2016-11-24 01:23:41','2016-11-24 01:23:41'),(33,1,'MonThu',1,15,6,'2016-11-24 01:24:05','2016-11-24 01:24:05'),(34,1,'WedSat',2,15,13,'2016-11-24 01:24:14','2016-11-24 01:24:14'),(35,1,'WedFri',3,15,4,'2016-11-24 01:24:27','2016-11-24 01:24:27'),(36,1,'MonWedThu',1,16,6,'2016-11-24 01:24:56','2016-11-24 01:24:56'),(37,1,'ThuFri',2,16,4,'2016-11-24 01:25:11','2016-11-24 01:25:11'),(38,1,'Sat',3,16,7,'2016-11-24 01:25:28','2016-11-24 01:25:28'),(39,1,'MonFriSat',1,17,3,'2016-11-24 01:26:03','2016-11-24 01:26:03'),(40,1,'FriSat',2,17,7,'2016-11-24 01:26:13','2016-11-24 01:26:13'),(41,1,'WedFri',3,17,4,'2016-11-24 01:26:24','2016-11-24 01:26:24'),(43,1,'MonWedThu',1,18,6,'2016-11-24 01:26:58','2016-11-24 01:26:58'),(44,1,'WedFriSat',2,18,4,'2016-11-24 01:27:09','2016-11-24 01:27:09'),(45,1,'TueSat',3,18,13,'2016-11-24 01:27:18','2016-11-24 01:27:18'),(46,1,'Fri',1,19,3,'2016-11-24 01:37:26','2016-11-24 01:37:26'),(47,1,'WedSat',2,19,4,'2016-11-24 01:37:35','2016-11-24 01:37:35');
/*!40000 ALTER TABLE `inscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institutions`
--

DROP TABLE IF EXISTS `institutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institutions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institutions`
--

LOCK TABLES `institutions` WRITE;
/*!40000 ALTER TABLE `institutions` DISABLE KEYS */;
INSERT INTO `institutions` VALUES (1,'Metrocamp - Faculdade Integrada Metropolitana','2016-11-03 12:19:35','2016-11-23 18:43:42'),(3,'UEV - University of Evansville','2016-11-05 21:20:12','2016-11-10 23:14:17'),(4,'PUCC - Pontifícia Universidade Católica de Campinas','2016-11-10 23:13:07','2016-11-10 23:13:19'),(5,'Unisal - Universidade Salesiana de Campinas','2016-11-10 23:14:48','2016-11-10 23:14:48');
/*!40000 ALTER TABLE `institutions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_07_19_011511_alter_users_add_column_type',1),('2016_08_14_004028_create_institutions_table',1),('2016_08_23_223654_create_courses_table',1),('2016_08_27_001921_create_subjects_table',1),('2016_08_29_164302_create_course_subject_table',1),('2016_10_04_224515_create_inscriptions_table',1),('2016_10_05_021530_alter_users_add_column_institution_id_ra',1),('2016_10_24_125058_alter_users_add_column_surname',1),('2016_11_01_221430_alter_subjects_add_column_req_num_ins',1),('2016_11_02_132658_add_default_users',1),('2016_11_02_143323_create_configurations_table',1),('2016_11_02_160012_create_dpclasses_table',1),('2016_11_02_185845_create_dpclass_user_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hours` int(11) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `req_num_ins` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (1,'Cálculo',80,1,'2016-11-03 12:47:15','2016-11-23 18:55:59',1),(2,'Engenharia de Software',80,1,'2016-11-10 18:10:15','2016-11-20 21:11:13',2),(3,'Patologia Geral',80,1,'2016-11-10 23:26:43','2016-11-24 00:59:34',3),(4,'Química Geral',80,1,'2016-11-10 23:30:51','2016-11-24 00:59:41',5),(5,'Termodinâmica',120,1,'2016-11-10 23:35:25','2016-11-24 01:00:34',5),(6,'Cálculo',80,1,'2016-11-10 23:37:37','2016-11-24 01:00:47',4),(7,'Direito Familiar',120,1,'2016-11-10 23:38:13','2016-11-24 01:04:20',5),(8,'Ciência Política',80,1,'2016-11-10 23:39:22','2016-11-24 01:03:06',3),(9,'Direito Processual Trabalhista',80,1,'2016-11-10 23:40:05','2016-11-24 01:04:45',5),(10,'Metodologia da Pesquisa em Direito',40,1,'2016-11-10 23:41:00','2016-11-24 01:04:31',3),(11,'Criminologia',40,1,'2016-11-10 23:41:52','2016-11-24 01:04:11',5),(12,'Interfaces Jornalísticas',80,1,'2016-11-10 23:42:50','2016-11-24 01:00:22',3),(13,'Metodologia de Pesquisa',40,1,'2016-11-10 23:44:16','2016-11-24 00:59:10',5),(14,'Trabalho Interdisciplinar de Graduação I: Jornalismo Móvel',80,1,'2016-11-10 23:44:52','2016-11-24 01:03:32',3),(15,'Radiojornalismo',80,1,'2016-11-10 23:45:34','2016-11-24 01:03:22',3),(16,'Poucos inscritos',99,1,'2016-11-19 15:32:57','2016-11-19 15:32:57',1),(18,'Teste',40,1,'2016-11-19 20:54:52','2016-11-19 20:54:52',5);
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) NOT NULL,
  `ra` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `institution_id` int(10) unsigned DEFAULT NULL,
  `surname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_institution_id_foreign` (`institution_id`),
  CONSTRAINT `users_institution_id_foreign` FOREIGN KEY (`institution_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Master','master@master.com','$2y$10$jWSbDrU01xefkAcXBnYkmesCFtnQ.HN6/eyuJHcefnhJd.qJGpiZa','mBSnbm0Hy6cl91mjF4waEYDHMaKoBEsuAwLAnF9GTRNyXEQXabbOs5T0qhF4',NULL,'2016-11-24 00:24:11',3,'',NULL,'CentralDP'),(11,'Carlos','carlos@carlos.com','$2y$10$OKkasy6CC9E08huTaHnZMO7bmMxtGj8DPEdGJdBablcdkCz9bJ27y','cyyU9EDkU2oYoZCYs7A1ogV1tO358rVKqjrVms1ItcP9fDKzw4Y1Z39EZIVY','2016-11-24 00:25:00','2016-11-24 01:17:11',1,'1510059873',1,'Pereira Neto'),(12,'Roberta','roberta@roberta.com','$2y$10$cgIteoP3O44mibA3/Nfew.hcE8CydkRKhtu4iJtJdWV6hFc36PoHu','PikTcdnYm8f5P0qrJ5p7cMAxxRhu5x1LJTd7ryIS5AFWoXyPEtps38oRS9tg','2016-11-24 00:25:40','2016-11-24 01:18:43',1,'1510012973',1,'Flores'),(13,'Paula','paula@paula.com','$2y$10$RFJ4slzyQ4WbBfJjM4aIH.PiPKHAX5kC77jAc.x35wiUqV7xHsngi','TfdUUuUKscFCaFurmuV2NW36TrBZFQzFrm0camr4oIgh3yE4f8jaqVppdVwS','2016-11-24 00:26:15','2016-11-24 01:22:23',1,'1510033692',1,'Martin'),(14,'Lucas','lucas@lucas.com','$2y$10$5jKlAMMTIcCViIg5STaoXeJXLsbZe4Mnjjoe4kCfZ6NXA0KvGVENK','H813uBDR1A3iHLz1bzsivV9krj6h4GqZ3jcO3mnCk3GN73ctLUgiMVAaDpnF','2016-11-24 00:26:38','2016-11-24 01:23:44',1,'1510069982',1,'da Vila'),(15,'Caio','caio@caio.com','$2y$10$IxW1YXg2IHhnuK6X10LiK.TuFRzZocLz1cliHD3B/AiQMjLc85QEa','PjWKBSy3BZLd6si10IB7RTmE9nU6230OHAixp4mEFjEz7EYduluWh4F3QF2C','2016-11-24 00:27:13','2016-11-24 01:24:37',1,'1510066159',1,'Oliveira'),(16,'Andre','andre@andre.com','$2y$10$zinJx0l274bTYSaQLZtAbOqvwBXowGXgycSgQ0BUNytCaPVj82Xai','k1HFLgPVWevB4liXpPjpwpoJtkXBs62IKwJZmFvbp0jZaKRGhYqFkDxNyZcS','2016-11-24 00:27:50','2016-11-24 01:25:33',1,'1510036985',1,'Moreira'),(17,'Jessica','jessica@jessica.com','$2y$10$yI/VjVadI6QCxPgCBgjrx.Z679E0fv/e1vjPQPK6sWichMsqsVLvC','IrmbDrkqhCtu4byvDx07bBpPpT55HYwtuwPLkYgw0rB2pRxbAV2ATdlFxleG','2016-11-24 00:28:19','2016-11-24 01:26:39',1,'1510074147',1,'da Costa'),(18,'Leonardo','leonardo@leonardo.com','$2y$10$.fEqQHKRDl.WXq4aQ0Vz7OYnU9nL/8ohqVwVXhylC6QsSuE0UCvRO','QK8yHE6uVqudlKMRji2JqPaFQMmlYjBeWOoLMHHlRAKjPk9Uufk8hOY9cN5U','2016-11-24 00:29:27','2016-11-24 01:27:21',1,'1510066321',1,'de Souza'),(19,'Silva','silvia@silvia.com','$2y$10$.STv4aQ4zUXVtGugP21hDuHGzY7TAqakviZ/X/mygMN82tP2D/iTi','RNxECajFSPRaomGNYfz9OM8LMCx9oKSUliCR0vZjuNgjtZSLt5sHzuRurScX','2016-11-24 00:30:02','2016-11-24 01:40:24',1,'1510045678',1,'Ramalho'),(20,'Josi','josi@josi.com','$2y$10$/e9Et9y2dZQoREe7bbM/Geuri2ow7BxakqFyLgn/DptzGz/gTVh4C','XNtAlgQkvwiTrFVNDf5CDXjqGxeuG3bwHDzYM49RxNwYe18FXWlNwWyBmUfP','2016-11-24 00:30:30','2016-11-24 00:30:42',1,'1510011478',1,'de Paula');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-24  1:48:15
