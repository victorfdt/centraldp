<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Task;

class User extends Authenticatable
{

    // Valid constant names
    const ROLE_STUDENT = 1;
    const ROLE_ADMIN = 2;
    const ROLE_MASTER = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'institution_id', 'ra', 'surname', 'max_num_ins'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tasks(){
        return $this->hasMany(Task);
    }

    /*
        Returns the inscriptions of the this user
    */
    public function inscriptions(){
       return $this->hasMany('App\Inscription');
    }

    /**
     * Returns that institution that the user belongs
     */
    public function institution(){
        return $this->belongsTo('App\Institution');
    }

    /**
     * Returns the DPClasses that the user is registerded
     */
    public function dpClasses()
    {
        return $this->belongsToMany('App\DPClass', 'dpclass_user', 'user_id', 'dpclass_id')->withTimestamps();
    }

    public function hasRole($role){
        $result = false;
        $role;
         if($this->type == $role){
             $result = true;
         }

         return $result;
    }

}
