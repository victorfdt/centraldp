<?php

namespace App;
/*
    This class have the summary information of an Inscription
*/
class SummarySubject
{
    private $subject;
    private $qtdInscriptions; 
    
    private $qtdPriority1;
    private $qtdPriority2;
    private $qtdPriority3;
    private $qtdPriority4;
    private $qtdPriority5;
    
    public function __construct($subjectId, $day) {
        //When receiving objects, you always need to set on $this
        $this->subject = Subject::find($subjectId);
        
        $this->qtdInscriptions = $this->subject->inscriptions()
                ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
                ->where('days', 'like', '%' . $day . '%')
                ->count();
            
        $this->qtdPriority1 = $this->subject->inscriptions()
                ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
                ->where('days', 'like', '%' . $day . '%')
                ->where('priority', '=', 1)
                ->count();
                
        $this->qtdPriority2 = $this->subject->inscriptions()
                ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
                ->where('days', 'like', '%' . $day . '%')
                ->where('priority', '=', 2)
                ->count();
                
        $this->qtdPriority3 = $this->subject->inscriptions()
                ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
                ->where('days', 'like', '%' . $day . '%')
                ->where('priority', '=', 3)
                ->count();
                
        $this->qtdPriority4 = $this->subject->inscriptions()
                ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
                ->where('days', 'like', '%' . $day . '%')
                ->where('priority', '=', 4)
                ->count();
                
        $this->qtdPriority5 = $this->subject->inscriptions()
                ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
                ->where('days', 'like', '%' . $day . '%')
                ->where('priority', '=', 5)
                ->count();
    }
    
    /* GET */
    public function getSubject() {
        return $this->subject;
    }
    
    public function getQtdInscriptions() {
        return $this->qtdInscriptions;
    }
    
    public function getQtdPriority1() {
        return $this->qtdPriority1;
    }
    
    public function getQtdPriority2() {
        return $this->qtdPriority2;
    }
    
    public function getQtdPriority3() {
        return $this->qtdPriority3;
    }
    
    public function getQtdPriority4() {
        return $this->qtdPriority4;
    }
    
    public function getQtdPriority5() {
        return $this->qtdPriority5;
    }
    
    /* SET */
    public function setSubject($subject) {
        $this->subject = $subject;
    }
    
    public function setQtdInscriptions($qtdInscriptions ) {
        $this->qtdInscriptions = qtdInscriptions;
    }
    
    public function setQtdPriority1($qtdPriority1) {
        $this->qtdPriority1 = $qtdPriority1;
    }
    
    public function setQtdPriority2($qtdPriority2) {
        $this->qtdPriority1 = $qtdPriority2;
    }
    
    public function setQtdPriority3($qtdPriority3) {
        $this->qtdPriority1 = $qtdPriority3;
    }
    
    public function setQtdPriority4($qtdPriority4) {
        $this->qtdPriority1 = $qtdPriority4;
    }
    
    public function setQtdPriority5($qtdPriority5) {
        $this->qtdPriority1 = $qtdPriority5;
    }
    
}
