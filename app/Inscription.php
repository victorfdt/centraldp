<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Inscription extends Model
{

    // Valid constant names
    const STATUS_OPEN = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_WAITING_DOCS = 3;
    const STATUS_RESOLVED = 4;
    const STATUS_CANCEL = 5;

    const MAX_NUM_INSCRIPTIONS = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'days', 'priority', 'user_id' , 'subject_id'
    ];

    /*
        Returns the subject that his inscriptions belongs
    */
    public function subject(){
        return $this->belongsTo('App\Subject');
    }

    /*
        Returns the user that this inscriptions belongs
    */
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function getStatusDescription(){
      $description = "";
      switch ($this->status) {
        case $this::STATUS_OPEN:
            $description =  "Aberta";
            break;
        case $this::STATUS_IN_PROGRESS:
            $description =  "Em andamento";
            break;
        case $this::STATUS_WAITING_DOCS:
            $description =  "Aguardando documentação";
            break;
        case $this::STATUS_RESOLVED:
            $description =  "Finalizada";
            break;
        default:
            $description = "ERRO";
        }
      
      return $description;
  }

    public function setInscriptionPriorityField($newPriority){

        $actualPriority = $this->priority;

        //Looking for the inscriptions of the user that have the same or bigger priority compared to the new one.
        $userInscriptions = $this->user()->inscriptions;

        //Getting all the available subjects
        $userInscriptions =


        DB::table('inscriptions')
            ->where('user_id', '=', $newPriority)
            ->where('priority', '=>', '1')
            ->select('courses.*')
            ->distinct()
            ->get();

    }

    /*
        This function gets the days Strings and creates an array which has the name of the selected days.
    */
    public function daysToStringArray(){
        $days = $this->days;
        $daysArray = [];

        if(substr_count($days, 'Mon') ){
            array_push($daysArray, "Segunda-feira");
        }

        if(substr_count($days, 'Tue') ){
            array_push($daysArray, "Terça-feira");
        }

        if(substr_count($days, 'Wed') ){
            array_push($daysArray, "Quarta-feira");
        }

        if(substr_count($days, 'Thu') ){
            array_push($daysArray, "Quinta-feira");
        }

        if(substr_count($days, 'Fri') ){
            array_push($daysArray, "Sexta-feira");
        }

        if(substr_count($days, 'Sat') ){
            array_push($daysArray, "Sábado");
        }

        return $daysArray;
    }
}
