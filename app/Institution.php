<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institution extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
    
    /**
        Returns the courses of the this institution
    */
    public function courses(){
       return $this->hasMany('App\Course');
    }
    
    /**
        Returns the users of the this institution
    */
    public function users(){
       return $this->hasMany('App\User');
    }
}
