<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'hours', 'available', 'req_num_ins'
    ];

    /**
     * The courses that belong to the subject.
     */
    public function courses()
    {
        return $this->belongsToMany('App\Course')->withTimestamps();
    }

    /*
        Returns the inscriptions of the subject
    */
    public function inscriptions(){
       return $this->hasMany('App\Inscription');
    }
}
