<?php

namespace App;
use SummarySubject;

/*
    This class have the summary information of an Inscription
*/
class SearchEngine
{
    private $summarySubject;
    function __construct() {
    }

    public function calcScore($summarySubject){
        $this->summarySubject = $summarySubject;

        //Calculating the priority
        $priorityPoints = ($summarySubject->getQtdPriority1() * 5
            + $summarySubject->getQtdPriority2() * 4
            + $summarySubject->getQtdPriority3() * 3
            + $summarySubject->getQtdPriority4() * 2
            + $summarySubject->getQtdPriority5() * 1)
            / 15;

        //Calculating the points of the quantity of inscriptions
        $qtdInscriptionsPoints = $summarySubject->getQtdInscriptions() / $summarySubject->getSubject()->req_num_ins;

        $score = ($priorityPoints * 2 + $priorityPoints * 1) / 3;

        return $score;
    }
}
