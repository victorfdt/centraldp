<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subject;

class DPClass extends Model
{

  // Valid constant names
    const STATUS_WAITING_DOCS = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_CLOSED = 3;
    const STATUS_CANCEL = 4;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'dpclasses';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'status', 'start_date', 'end_date', 'professor_name', 'professor_email', 'description', 'days', 'subject_id'
  ];

  /**
   * Return the users that are registered
   */
  public function users()
  {
      return $this->belongsToMany('App\User', 'dpclass_user', 'dpclass_id','user_id')->withTimestamps();
  }

  /*
    This function returns the subject of the DPClass
  */
  public function subject(){
      return Subject::find($this->subject_id);
  }
  
  public function getStatusDescription(){
      $description = "";
      switch ($this->status) {
        case $this::STATUS_WAITING_DOCS:
            $description =  "Aguardando documentação";
            break;
        case $this::STATUS_IN_PROGRESS:
            $description =  "Em andamento";
            break;
        case $this::STATUS_CLOSED:
            $description =  "Encerrada";
            break;
        case $this::STATUS_CANCEL:
            $description =  "Cancelada";
            break;
        default:
            $description = "ERRO";
        }
      
      return $description;
  }
  
  /*
        This function gets the days Strings and creates an array which has the name of the selected days.
    */
    public function daysToStringArray(){
        $days = $this->days;
        $daysArray = [];

        if(substr_count($days, 'Mon') ){
            array_push($daysArray, "Segunda-feira");
        }

        if(substr_count($days, 'Tue') ){
            array_push($daysArray, "Terça-feira");
        }

        if(substr_count($days, 'Wed') ){
            array_push($daysArray, "Quarta-feira");
        }

        if(substr_count($days, 'Thu') ){
            array_push($daysArray, "Quinta-feira");
        }

        if(substr_count($days, 'Fri') ){
            array_push($daysArray, "Sexta-feira");
        }

        if(substr_count($days, 'Sat') ){
            array_push($daysArray, "Sábado");
        }

        return $daysArray;
    }
    
}
