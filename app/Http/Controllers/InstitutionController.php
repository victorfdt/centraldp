<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Institution;
use Session;

class InstitutionController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        //Master role
        $this->middleware('role:3');
    }

    /**
     * Index page of the Instituion.
     *
     * @return void
     */
    public function index() {
        $institutions = Institution::all();

        return view('dashboard/institution.index', [
            'institutions' => $institutions,
        ]);
    }

    public function create() {
        return view('dashboard/institution.create');
    }

    /*
      Creates a new Instituion
     */
    public function store(Request $request) {
        //Validation
        $this->validate($request, [
            'name' => 'required|unique:institutions|max:100'
        ]);

        $institution = new Institution();
        $institution->name = $request->name;
        $institution->save();

        Session::flash('success_message', 'A instituição '. $institution->name . '  foi criada com sucesso!');

        return redirect('/dashboard/institution');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $institution = Institution::find($id);

        return view('dashboard/institution.edit', [
            'institution' => $institution,
        ]);
    }

    /**
     * Destroy the given task.
     *
     * @param  Request  $request
     * @param  Task  $task
     * @return Response
     */
    public function destroy($id) {
        $institution = Institution::find($id);
        $name = $institution->name;
        $institution->delete();

        Session::flash('success_message', 'A instituição '. $name . ' foi removida com sucesso!');

        return redirect('/dashboard/institution');
    }

     /**
     * Update the institution.
     *
     * @param  Request  $request
     * @param  $id
     * @return Response
     */
    public function update(Request $request, $id){
        //Validation
        $this->validate($request, [
            'name' => 'required|max:100'
        ]);

        //Find the institution that will be updated
        $institution = Institution::find($id);

        $institution->name = $request->name;
        $institution->save();

        Session::flash('success_message', 'A instituição '. $institution->name . ' foi alterada com sucesso!');

        return redirect("/dashboard/institution");
    }

    /*
        This function displays the institution to be selected as current institution.
    */
    public function editCurrentInstitution(){
        $institutions = Institution::all();
        
        return view('dashboard/institution.edit_current_institution', [
            'institutions' => $institutions,
        ]);
    }

    /*

    */
    public function setCurrentInstitution(Request $request){
        //Getting the selected institution
        $institution = Institution::find($request->institution);

        //Setting the institution on the session
        $request->session()->put('currentInstitution', $institution);

        //Success message
        Session::flash('success_message', 'A instituição ' . $institution->name .  ' foi selecionada como corrente no sistema.');

        return redirect("/dashboard/institution/editCurrentInstitution");
    }

}
