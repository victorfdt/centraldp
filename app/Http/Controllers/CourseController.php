<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Institution;
use App\Course;
use Session;
use DB;

class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        //Master role
        $this->middleware('role:3');
        
        //Checking if its selected the current institution
        $this->middleware('checkCurrentInstitution');
    }

    /**
     * Index page of the Course
     *
     * @return void
     */
    public function index() {
        //Getting all the courses from the current institution
        $institution = Session::get('currentInstitution');

        //If there is not a current institution, the user will be sent the selection page
        if($institution == null){
            Session::flash('info_message', 'Para acessar a area de cursos, você deve primeiro selecionar uma instituição.');

            return redirect('/dashboard/institution/editCurrentInstitution');
        }

        $courses = $institution->courses()->get();

        return view('dashboard/course.index', [
            'courses' => $courses
        ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function store(Request $request) {
        //Validation messages
        $messages = [
            'name.required' => 'O campo Nome é obrigatório.',
            'name.max:100' => 'O campo Nome não pode ter mais de 100 caracteres.'
        ];

        //Validation
        //TODO 'name' => 'required|unique:courses|max:100'
        $this->validate($request, [
            'name' => 'required|max:100'
        ], $messages);

        $course = new Course();
        $course->name = $request->name;
        $course->period = $request->period;
        $course->institution_id = $request->session()->get('currentInstitution.id');
        $course->save();

        Session::flash('success_message', 'O curso '. $course->name . '  foi criado com sucesso!');

        return redirect('/dashboard/course/');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function create(Request $request) {
        $institution = $request->session()->get('currentInstitution');

        return view('dashboard/course.create', [
            'institution' => $institution,
        ]);
    }

    /**
     * Load the edition page of the course.
     *
     * @return void
     */
    public function edit($id) {
        $course = Course::find($id);

        //Getting the institution of this course
        $institution = $course->institution;
        $institutions = Institution::all();

        return view('dashboard/course.edit', [
            'course' => $course,
            'institution' => $institution,
            'institutions' => $institutions,
        ]);

    }

    /**
     * This function updates the course
     * @param $request
     * @param $id course id
     *
     * @return void
     */
    public function update(Request $request, $id) {
        //Validation messages
        $messages = [
            'name.required' => 'O campo Nome é obrigatório.',
            'name.max:100' => 'O campo Nome não pode ter mais de 100 caracteres.'
        ];

        //Validation
        $this->validate($request, [
            'name' => 'required|max:100'
        ], $messages);

        $course = Course::find($id);
        $oldInstitutionId = $course->institution_id;

        //Setting the new information
        $course->name = $request->name;
        $course->period = $request->period;

        $course->save();

        //Success message
        Session::flash('success_message', 'O curso ' . $course->name . ' foi alterado com sucesso!');

        return redirect("/dashboard/course/");
    }

    /**
     * Remove a course by a given id.
     *
     * @return void
     */
    public function destroy($id) {
        $course = Course::find($id);
        $courseName = $course->name;
        $course->delete();

        Session::flash('success_message', 'O curso '. $courseName . ' foi removido com sucesso!');

        return redirect('/dashboard/course/');
    }


}
