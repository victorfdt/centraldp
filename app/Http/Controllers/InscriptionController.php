<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Inscription;
use App\Course;
use App\Subject;
use Validator;
use Session;
use Auth;
use DB;

class InscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        //Type 1 = student
        $this->middleware('role:1')->except(['ajaxGetSubjectsByCourse', 'ajaxGetCoursesByInstitution']);
    }

    /**
     * Index page of the Inscription.
     *
     * @return void
     */
    public function index() {
        $user = Auth::user();
        $inscriptions = $user->inscriptions()->orderBy('priority', 'asc')->get();

        return view('dashboard/inscription.index', [
            'inscriptions' => $inscriptions,
        ]);
    }

    /*
     This function returns the creating inscription page
    */
    public function create() {
        //Logged user
        $user = Auth::user();
        
        //Checking how much inscriptions the user have
        $qtdInscription = $user->inscriptions()->count();
        
        if($qtdInscription >= Inscription::MAX_NUM_INSCRIPTIONS){
            $hasError = true;
            Session::flash('info_message', 'Não é possível realizar mais inscrições pois já foram cadastradas o número máximo de ' . Inscription::MAX_NUM_INSCRIPTIONS);

            return redirect('/dashboard/inscription/');
        }

        //Institution name
        $institution = $user->institution;
        $institutionName = $institution->name;
        
        $courses = $institution->courses()->get();
        
        $hasSubject = true;

        //Getting all the available subjects
        $subjects = DB::table('institutions')
            ->join('courses', 'institutions.id', '=', 'courses.institution_id')
            ->join('course_subject', 'courses.id', '=', 'course_subject.course_id')
            ->join('subjects', 'course_subject.subject_id', '=', 'subjects.id')
            ->where('institutions.id', '=', $user->institution->id)
            ->where('subjects.available', '=', '1')
            ->select('subjects.*')
            ->distinct()
            ->get();

        //If it was not found any subject, it will be shown an information message to the user
        if(sizeof($subjects) <= 0){
            $hasSubject = false;

            Session::flash('info_message', 'Não há disciplinas disponíveis para inscrição. Por favor, aguarde até
            que a administração disponibilize uma disciplina.');

            return view('dashboard/inscription.index');
        }

        return view('dashboard/inscription.create', [
            'institutionName' => $institutionName,
            'courses' => $courses,
            'hasSubject' => $hasSubject
        ]);
    }

    /*
        Creates a new inscription
    */
    public function store(Request $request){
        //Validation
        $validator = Validator::make($request->all(), [
            'subject' => 'required',
            'course' => 'required'
        ]);

        //Logged user
        $user = Auth::user();
        $hasError = false;

        //This will be a string that will be composed by picies of text that will discribe the selected days.
        //For example: MonFriSat, which means Monday, Friday and Saturday.
        $selectedDays = "";

        if(isset($request->monday)){
            $selectedDays .= 'Mon';
        }

        if(isset($request->tuesday)){
            $selectedDays .= 'Tue';
        }

        if(isset($request->wednesday)){
            $selectedDays .= 'Wed';
        }

        if(isset($request->thursday)){
            $selectedDays .= 'Thu';
        }

        if(isset($request->friday)){
            $selectedDays .= 'Fri';
        }

        if(isset($request->saturday)){
            $selectedDays .= 'Sat';
        }

        //Checking if it was selected at least one day
        if($selectedDays == ""){
            $hasError = true;
            $validator->errors()->add('days', 'Você deve selecionar pelo menos um dia.');
        }

        //Checking if the user already has selected this subject before.
        //The user can only assign for a subject only one time.
        $alreadyRegister = Inscription::where('user_id', '=', $user->id)
                            ->where('subject_id', '=', $request->subject)->get();

        if(count($alreadyRegister) > 0){
            $hasError = true;
            $subjectName = Subject::find($request->subject)->name;
            $validator->errors()->add('subject', 'Não é permitido inscrever-se na disciplina
                        ' . $subjectName . ', pois você já possui uma inscrição com essa disciplina.');
        }
        
        if($hasError){
            return redirect('/dashboard/inscription/create')
                ->withErrors($validator)
                ->withInput();
        }

        //New inscription will receive the last priority
        $priority = sizeof($user->inscriptions()->get()) + 1;

        //Creating a new inscription
        $inscription = new Inscription();
        $inscription->user_id = $user->id;
        $inscription->subject_id = $request->subject;
        $inscription->status = Inscription::STATUS_OPEN;
        $inscription->priority = $priority;
        $inscription->days = $selectedDays;

        $inscription->save();

        //Success message
        Session::flash('success_message', 'A inscrição da disciplina ' . $inscription->subject->name .  ' foi criada com sucesso!');

        return redirect('/dashboard/inscription');
    }

    /**
     * Load the edition page of the inscription.
     *
     */
    public function edit($id){
        $inscription = Inscription::find($id);
        
        //Checking the status of the inscription
        if($inscription->status != Inscription::STATUS_OPEN){
            Session::flash('info_message', 'Não é possível alterar inscrições que tenham o status diferente de Aberta.');

            return redirect('/dashboard/inscription');
        }
        
        $subjectName = $inscription->subject->name;
        $user = Auth::user();
        $institutionName = $user->institution->name;
        $days = $inscription->days;

        $daysArray = [];

        //Checking the selected days
        if(substr_count($days, 'Mon')){
            $daysArray['monday'] = true;
        } else {
            $daysArray['monday'] = false;
        }

        if(substr_count($days, 'Tue')){
            $daysArray['tuesday'] = true;
        } else {
            $daysArray['tuesday'] = false;
        }

        if(substr_count($days, 'Wed')){
            $daysArray['wednesday'] = true;
        } else {
            $daysArray['wednesday'] = false;
        }

        if(substr_count($days, 'Thu')){
            $daysArray['thursday'] = true;
        } else {
            $daysArray['thursday'] = false;
        }

        if(substr_count($days, 'Fri')){
            $daysArray['friday'] = true;
        } else {
            $daysArray['friday'] = false;
        }

        if(substr_count($days, 'Sat')){
            $daysArray['saturday'] = true;
        } else {
            $daysArray['saturday'] = false;
        }

        //Finding the number os inscriptions
        $numInscriptions = $user->inscriptions()->count();

        return view('dashboard/inscription.edit', [
            'subjectName' => $subjectName,
            'inscription' => $inscription,
            'institutionName' => $institutionName,
            'daysArray' => $daysArray,
            'numInscriptions' => $numInscriptions,
        ]);
    }

    /**
     * Load the edition page of the inscription's priority.
     *
     */
    public function priority(){
        $user = Auth::user();
        $inscriptions = $user->inscriptions()->orderBy('priority', 'asc')->get();

        //It will be shown a message if there are not inscriptions
        if(sizeof($inscriptions) <= 0){
            Session::flash('info_message', 'Não há inscrições de disciplina disponíves para que seja feita a alteração de prioridade.');

            return view('dashboard/inscription.index');
        }

        return view('dashboard/inscription.edit_priority', [
            'inscriptions' => $inscriptions,
        ]);
    }

    /**
     * Load the edition page of the inscription's priority.
     *
     */
    public function priorityUpdate(Request $request){
        $user = Auth::user();

        //Converting the string with the inscriptions Ids to an array
        $inscriptionsIdByPriorityOrder = explode(",", $request->inscriptionsOrder);

        for($i = 0; $i < sizeof($inscriptionsIdByPriorityOrder); $i++){
            $inscription = Inscription::find($inscriptionsIdByPriorityOrder[$i]);
            $newPriority = $i + 1;
            $inscription->priority = $newPriority;
            $inscription->save();
        }

        Session::flash('success_message', 'As prioridades das inscrições foram atualizadas com sucesso!');

        return redirect('/dashboard/inscription');
    }

    /*
        This function will update the inscription
    */
    public function update(Request $request, $id){
        //Validation
        $validator = Validator::make($request->all(), []);

        //Logged user
        $user = Auth::user();
        $hasError = false;

        //This will be a string that will be composed by pieces of text that will discribe the selected days.
        //For example: MonFriSat, which means Monday, Friday and Saturday.
        $selectedDays = "";

        if(isset($request->monday)){
            $selectedDays .= 'Mon';
        }

        if(isset($request->tuesday)){
            $selectedDays .= 'Tue';
        }

        if(isset($request->wednesday)){
            $selectedDays .= 'Wed';
        }

        if(isset($request->thursday)){
            $selectedDays .= 'Thu';
        }

        if(isset($request->friday)){
            $selectedDays .= 'Fri';
        }

        if(isset($request->saturday)){
            $selectedDays .= 'Sat';
        }

        //Checking if it was selected at least one day
        if($selectedDays == ""){
            $hasError = true;
            $validator->errors()->add('days', 'Você deve selecionar pelo menos um dia para cursar a disciplina.');
        }

        if($hasError){
            return redirect('/dashboard/inscription/create')
                ->withErrors($validator)
                ->withInput();
        }

        //Updating the inscription
        $inscription = Inscription::find($id);
        $inscription->days = $selectedDays;
        $inscription->save();

        //Success message
        Session::flash('success_message', 'A inscrição da disciplina ' . $inscription->subject->name .  ' foi atualizada com sucesso!');

        return redirect('/dashboard/inscription');
    }

    /**
     * Remove a inscription.
     *
     * @return void
     */
    public function destroy($id) {
        $inscription = Inscription::find($id);
        $subjectName = $inscription->subject->name;
        $user = Auth::user();

        //Getting the user's inscription that have greater priority than the selected priority.
        //Each one of these inscriptions will have its priority decreased by 1.
        $inscriptionsToUpdate = Inscription::where('priority', '>', $inscription->priority)->get();

        foreach($inscriptionsToUpdate as $ins){
            $ins->priority = $ins->priority - 1;
            $ins->save();
        }

        //Removing selected inscription
        $inscription->delete();

        Session::flash('success_message', 'A inscrição da disciplina '. $subjectName . ' foi removida com sucesso!');

        return redirect('/dashboard/inscription/');
    }
    
    /**
     * Returns the courses of the given institution id using ajax
     *
     * @return void
     */
    public function ajaxGetCoursesByInstitution($id) {
        $institution = Institution::find($id);
        $courses = $institution->courses()->orderBy('name', 'asc')->get();

        return response()->json(array('courses'=> $courses), 200);
    }

    /**
     * Returns the subjects of the given course id using ajax.
     *
     * @return void
     */
    public function ajaxGetSubjectsByCourse($id) {
        $course = Course::find($id);
        $subjects = $course->subjects()->orderBy('name', 'asc')->get();
        
        return response()->json(array('subjects'=> $subjects), 200);
    }

}
