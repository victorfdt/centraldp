<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Task;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a list of all of the user's task.
     *
     * @param  Requset  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tasks = $request->user()->tasks()->get();

        return view('tasks.index', [
            'tasks' => $tasks,
        ]);
    }
    
    public function search(Request $request){
        
        print_r($request);
        die();
        
        $this->validate($request, [
            'search' => 'required'
        ]);
        
        $tasks = Task::where('description', 'LIKE', '%'.$request->search.'%')->get();
        
        return view('tasks.index', [
            'tasks' => $tasks,
        ]);
    }
    
    public function show(Request $request){
        return redirect('/task');
    }
    
    public function edit($id){
        
        $task = Task::find($id);
        
        return view('tasks.edit', [
            'task' => $task,
        ]);
    }
    
    public function update(Request $request, Task $task){
        
       
        $validator = $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required'
        ]);
            // store
            $task = Task::find($task->id);
            $task->name             = $request->name;
            $task->description      = $request->description;
            $task->save();
    
            // redirect
            //Session::flash('message', 'Successfully updated Task!');
            return redirect('/task');
    }
    
    
    /**
     * Destroy the given task.
     *
     * @param  Request  $request
     * @param  Task  $task
     * @return Response
     */
    public function destroy(Request $request, Task $task)
    {
        $task->delete();
    
        return redirect('/task');
    }
    
    public function create(Request $request){
        return view('tasks.create');
    }
    
    /**
     * Create a new task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required'
        ]);
        
        /** 
        $task = new Task();
        $task->name = $request->name;
        $task->description = $request->description;
        $task->save();
        */
        
        $request->user()->tasks()->create([
            'name' => $request->name,
            'description' => $request->description
        ]);
        
        return redirect('/task');
    }
    
}
