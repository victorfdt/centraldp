<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Inscription;
use App\DPClass;
use App\Subject;
use App\User;
use Session;
use Auth;
use DB;

class DPClassController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        
        //Master role
        $this->middleware('role:3')->except(['viewClass']);
        
        //Checking if its selected the current institution
        $this->middleware('checkCurrentInstitution')->except(['viewClass']);
    }

    /**
     * Index page of the DPClass
     *
     * @return void
     */
    public function index() {
        //Getting the current institution
        $institution = Session::get('currentInstitution');

        $courses = $institution->courses()->get();

        return view('dashboard/dpclass.index', [
            'institution' => $institution,
            'courses' => $courses
        ]);
    }

    /*
        Searching the DPClass by the form
    */
    public function search(Request $request){
        //Getting the current institution
        $institution = Session::get('currentInstitution');

        $courses = $institution->courses()->get();

        //Getting the information provided on the form
        $courseId = $request->course;
        $subjectId = $request->subject;
        $professorName = $request->professorName;
        $studentName = $request->studentName;

        //Collection of DPClass objects that will be send to the view
        $dpclasses = array();
        
        //If the user wants to find the dpclass on all subjects
        if($courseId == -1 && $subjectId == -1){
            $dpclasses = DPClass::join('dpclass_user', 'dpclasses.id', '=', 'dpclass_user.dpclass_id')
                ->join('users', 'dpclass_user.user_id', '=', 'users.id')
                ->where('dpclasses.professor_name', 'like', '%' . $professorName . '%')
                ->where('users.name', 'like', '%' . $studentName . '%')
                ->select('dpclasses.*')
                ->distinct()
                ->get();
        }
        
        //If the user wants to find all the DPClass by a course
        if($courseId != -1 && $subjectId == -1){
            $dpclasses = DPClass::join('dpclass_user', 'dpclasses.id', '=', 'dpclass_user.dpclass_id')
                ->join('users', 'dpclass_user.user_id', '=', 'users.id')
                ->join('subjects', 'dpclasses.subject_id', '=', 'subjects.id')
                ->join('course_subject', 'subjects.id', '=', 'course_subject.subject_id')
                ->join('courses', 'course_subject.course_id', '=', 'courses.id')
                ->where('dpclasses.professor_name', 'like', '%' . $professorName . '%')
                ->where('users.name', 'like', '%' . $studentName . '%')
                ->where('courses.id', '=', $courseId)
                ->select('dpclasses.*')
                ->distinct()
                ->get();
        }
        
        //If the subject was selected
        if($subjectId != -1){
            $dpclasses = DPClass::join('dpclass_user', 'dpclasses.id', '=', 'dpclass_user.dpclass_id')
                ->join('users', 'dpclass_user.user_id', '=', 'users.id')
                ->join('subjects', 'dpclasses.subject_id', '=', 'subjects.id')
                ->where('dpclasses.professor_name', 'like', '%' . $professorName . '%')
                ->where('users.name', 'like', '%' . $studentName . '%')
                ->where('subjects.id', '=', $subjectId)
                ->select('dpclasses.*')
                ->distinct()
                ->get();
        }
       
        if(count($dpclasses) == 0){
            Session::flash('info_message', 'Não foram encontradas turmas de DP através das informações dadas.');
                return view('dashboard/dpclass.index', [
                'courses' => $courses,
                'dpclasses' => $dpclasses,
                'professorName' => $professorName,
                'studentName' => $studentName
            ]);
        }

        return view('dashboard/dpclass.index', [
            'courses' => $courses,
            'dpclasses' => $dpclasses,
            'professorName' => $professorName,
            'studentName' => $studentName
        ]);
    }

    /**
     * Index page of the DPClass
     *
     * @return void
     */
    public function create() {
        //Getting the isntitution from the session
        $institution = Session::get('currentInstitution');

        $courses = $institution->courses()->get();

        return view('dashboard/dpclass.create', [
            'courses' => $courses
        ]);
    }
    
    /**
     * Edit page of the DPClass
     *
     * @return void
     */
    public function edit($id) {
        //Fining the DPClass by id
        $dpClass = DPClass::find($id);
        
        $availableStudents = array();
        
        $days = $dpClass->days;
        $daysArray = [];

        //Checking the selected days
        if(substr_count($days, 'Mon')){
            $daysArray['monday'] = true;
        } else {
            $daysArray['monday'] = false;
        }

        if(substr_count($days, 'Tue')){
            $daysArray['tuesday'] = true;
        } else {
            $daysArray['tuesday'] = false;
        }

        if(substr_count($days, 'Wed')){
            $daysArray['wednesday'] = true;
        } else {
            $daysArray['wednesday'] = false;
        }

        if(substr_count($days, 'Thu')){
            $daysArray['thursday'] = true;
        } else {
            $daysArray['thursday'] = false;
        }

        if(substr_count($days, 'Fri')){
            $daysArray['friday'] = true;
        } else {
            $daysArray['friday'] = false;
        }

        if(substr_count($days, 'Sat')){
            $daysArray['saturday'] = true;
        } else {
            $daysArray['saturday'] = false;
        }
        
        //Getting available students
        $subject = $dpClass->subject();
        $inscriptions = $subject->inscriptions()
            ->where('inscriptions.status', '=', Inscription::STATUS_OPEN )
            ->get();
        
        //Getting the current students
        $availableStudents = DB::table('inscriptions')
            ->join('users', 'inscriptions.user_id', '=', 'users.id')
            ->where('inscriptions.subject_id', '=', $subject->id)
            ->where('inscriptions.status', '=', Inscription::STATUS_OPEN)
            ->select('users.*')
            ->orderBy('users.name', 'asc')
            ->get();
       
        
        //Finding the students
        /* Using the AJAX method
        foreach($inscriptions as $inscription){
            $insc = Inscription::find($inscription->id);
            $availableStudents[] = array('priority' => $insc->priority, 'student' => $insc->user()->first());
        }
        */
       
        //Getting the current students
        $students = DB::table('dpclasses')
            ->join('dpclass_user', 'dpclasses.id', '=', 'dpclass_user.dpclass_id')
            ->join('users', 'dpclass_user.user_id', '=', 'users.id')
            ->where('dpclasses.id', '=', $id)
            ->select('users.*')
            ->orderBy('users.name', 'asc')
            ->get();
            
        return view('dashboard/dpclass.edit', [
            'dpClass' => $dpClass,
            'daysArray' => $daysArray,
            'availableStudents' => $availableStudents,
            'students' => $students
        ]);
    }

    /*
        This function stores a new DPClass
    */
    public function store(Request $request){
        //Validation
         $this->validate($request, [
            'professorName' => 'required|max:100',
            'professorEmail' => 'required|email',
            'description' => 'required',
            'days' => 'required'
        ]);

        $hasError = false;

        //This will be a string that will be composed by picies of text that will discribe the selected days.
        //For example: MonFriSat, which means Monday, Friday and Saturday.
        $selectedDays = "";

        foreach($request->days as $day){
            $selectedDays .= $day;
        }

        //Finding the selected subject
        $subject = Subject::find($request->subject);

        //Checking if there is the required number os students for that subject
        if(count($request->selectedStudents) < $subject->req_num_ins){
            Session::flash('error_message', 'São necessários ao menos ' . $subject->req_num_ins . ' alunos para criar uma turma de
            dependência da disciplina de ' . $subject->name);
            return redirect('/dashboard/dpclass/create');
        }

        //Getting the institution from the session
        $institution = Session::get('currentInstitution');

        //Creating the DPClass
        $dpClass = new DPClass();
        $dpClass->status = DPClass::STATUS_WAITING_DOCS;
        $dpClass->professor_name = $request->professorName;
        $dpClass->professor_email = $request->professorEmail;
        $dpClass->days = $selectedDays;
        $dpClass->description = $request->description;
        $dpClass->subject_id = $request->subject;

        $dpClass->save();

        //For each selected student, it will be created a record at pivot table
        foreach($request->selectedStudents as $studentId){
            
            //Attaching this user to the DPClass
            $dpClass->users()->attach($studentId);
            
            //Changing the status of the inscription of the user to WAITING_DOCS
            $student = User::find($studentId);
            $inscription = $student->inscriptions()->where('subject_id', '=', $subject->id)->first();
            $inscription->status = Inscription::STATUS_WAITING_DOCS;
            
            $inscription->save();
        }

        //Success message
        Session::flash('success_message', 'A turma de DP referente à disciplina ' . $subject->name .  ' foi criada com sucesso!');

        return redirect('/dashboard/dpclass');
    }
    
    /*
        This function updates a DPClass
    */
    public function update(Request $request, $id){
        //Validation
         $this->validate($request, [
            'professorName' => 'required|max:100',
            'professorEmail' => 'required|email',
            'selectedStudents' => 'required',
            'description' => 'required',
            'days' => 'required'
        ]);

        $hasError = false;

        //This will be a string that will be composed by picies of text that will discribe the selected days.
        //For example: MonFriSat, which means Monday, Friday and Saturday.
        $selectedDays = "";

        foreach($request->days as $day){
            $selectedDays .= $day;
        }

        $dpClass = DPClass::find($id);
        $subject = Subject::find($dpClass->subject_id);

        //Checking if there is the required number os students for that subject
        if(count($request->selectedStudents) < $subject->req_num_ins){
            Session::flash('error_message', 'São necessários ao menos ' . $subject->req_num_ins . ' alunos para criar uma turma de
            dependência da disciplina de ' . $subject->name);
            return redirect('/dashboard/dpclass/edit/'. $dpClass->id);
        }

        //Getting the institution from the session
        $institution = Session::get('currentInstitution');

        //Updating the DPClass
        $dpClass->professor_name = $request->professorName;
        $dpClass->professor_email = $request->professorEmail;
        $dpClass->days = $selectedDays;
        $dpClass->description = $request->description;
        $dpClass->status = $request->status;
        
        //Removing all the related courses from the subject.
        //The next step will add the new selected courses
        $dpClass->users()->detach();
        
        $dpClass->save();
        
        //For each selected student, it will be created a record at pivot table
        foreach($request->selectedStudents as $studentId){
            
            //Attaching this user to the DPClass
            $dpClass->users()->attach($studentId);
            
            //Changing the status of the inscription
            $student = User::find($studentId);
            $inscription = $student->inscriptions()->where('subject_id', '=', $subject->id)->first();
            
            //Checking the status
            if($request->status == DPClass::STATUS_IN_PROGRESS){
                $inscription->status = Inscription::STATUS_RESOLVED;
            }elseif($request->status == DPClass::STATUS_WAITING_DOCS){
                $inscription->status = Inscription::STATUS_WAITING_DOCS;
            }
            
            $inscription->save();
        }
        
        //Setting the available students
        if(isset($request->availableStudents) && count($request->availableStudents) > 0){
            foreach($request->availableStudents as $studentId){
                
                //Changing the status of the inscription
                $student = User::find($studentId);
                $inscription = $student->inscriptions()->where('subject_id', '=', $subject->id)->first();
                
                if(isset($inscription)){
                    $inscription->status = Inscription::STATUS_OPEN;
                    $inscription->save();
                }
                
            }
        }

        //Success message
        Session::flash('success_message', 'A turma de DP referente à disciplina ' . $subject->name .  ' foi alterada com sucesso!');

        return redirect('/dashboard/dpclass');
    }

    /*
        This function returns the registed students from inscriptions by subject id.
        The response will be by ajax
    */
    public function ajaxGetStudentsBySubject($subjectId){
        $students = $this->buildPriorityStudentArray($subjectId);
        
        //Sorting the students by name
        usort($students, $this->sortStudentsByName('student'));
        
        $qtdInscriptions = 0;

        return response()->json(array('students'=> $students, 'qtdInscriptions' => $qtdInscriptions), 200);
    }
    
    public function ajaxGetStudentById($userId){
        $student = User::find($userId);
        
        return response()->json(array('student'=> $student), 200);
    }
    
    /*
        This function create an array with the folling configuration
        array['priority' = value, 'student' => User]
    */
    private function buildPriorityStudentArray($subjectId){
        //Creating the new array of students
        $students = array();
        
        $subject = Subject::find($subjectId);
        $inscriptions = $subject->inscriptions()
            ->where('inscriptions.status', '=', Inscription::STATUS_OPEN )
            ->get();
        

        //Counting the number os inscriptions
        $qtdInscriptions = count($inscriptions);

        //Finding the students
        foreach($inscriptions as $inscription){
            $insc = Inscription::find($inscription->id);
            $students[] = array('priority' => $insc->priority, 'student' => $insc->user()->first());
        }
        
        return $students;
    }
    
    /*
        This function deletes a DPClass by a given DPClass id
    */
    public function destroy($id){
        
        //Getting the DPClass by the id
        $dpClass = DPClass::find($id);
       
        //Getting the name of the subject
        $subjectName = $dpClass->subject()->name;
        
        
        //If deleting a dpclass that is not closed,
        //the status of the inscriptions of the users will be in progress
        if($dpClass->status != DPClass::STATUS_CLOSED){
            $users = $dpClass->users()->get();
            
            foreach($users as $user){
                $inscription = $user->inscriptions()->where('subject_id', '=', $dpClass->subject_id)->first();
                $inscription->status = Inscription::STATUS_OPEN;
                $inscription->save();
            }
        }
        
        //Deliting the DPClass
        $dpClass->delete();
        
        //Success message
        Session::flash('success_message', 'A turma de DP referente à disciplina ' . $subjectName .  ' foi removida com sucesso!');
        
        return redirect('/dashboard/dpclass');
    }
    
    /*
        This function returns all the DPClass that the user is assigned
    */
    public function viewClass(){
        
        //Fiding the user
        $user = Auth::user();
        
        $dpClasses = $user->dpClasses()->get();
        
        return view('dashboard/dpclass.view_class', [
            'dpClasses' => $dpClasses,
        ]);
    }

    /*
    This function will sort a given array in DESC order using the given key
    */
    private function sortStudentsByName($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key]->name,$b[$key]->name);
        };
    }
}
