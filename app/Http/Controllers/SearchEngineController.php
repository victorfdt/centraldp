<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Subject;
use App\Institution;
use App\Inscription;
use App\SearchEngine;
use App\SummarySubject;
use Session;
use DB;

class SearchEngineController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct() {
        $this->middleware('auth');

        //Master role
        $this->middleware('role:3');

        //Checking if its selected the current institution
        $this->middleware('checkCurrentInstitution');
    }

    /**
    * Index page of the Subject
    *
    * @return void
    */
    public function index() {

        $institution = Session::get('currentInstitution');

        //Getting the courses of the current institution
        $courses = $institution->courses()->get();

        //Getting the quantity o registered subjects.
        $qtdSubjects = DB::table('institutions')
        ->join('courses', 'institutions.id', '=', 'courses.institution_id')
        ->join('course_subject', 'courses.id', '=', 'course_subject.course_id')
        ->join('subjects', 'course_subject.subject_id', '=', 'subjects.id')
        ->where('institutions.id', '=', $institution->id)
        ->select('subjects.*')
        ->count();

        $searchType = 'conventional';

        return view('dashboard/searchEngine.index', [
            'courses' => $courses,
            'qtdSubjects' => $qtdSubjects,
            'searchType' => $searchType
        ]);
    }

    public function create(){
        //Getting the institution from session
        $institution = Session::get('currentInstitution');

        //Getting the courses of the current institution
        $courses = $institution->courses()->get();

        return view('dashboard/searchEngine.create', [
            'courses' => $courses
        ]);
    }

    /*
    This function receives the subject position, the subject Id and the $day ('Mon').
    Furthermore, it will find all students and display them on the show page
    */
    public function show($position, $subjectId, $day){
        //Getting the institution from session
        $institution = Session::get('currentInstitution');
        $students = array();

        //Getting the subject
        $subject = Subject::find($subjectId);

        //Getting the inscriptions of the subject on the spefied day
        $inscriptions = $subject->inscriptions()
            ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
            ->where('days', 'like', '%' . $day . '%')
            ->get();

        //Counting the number os inscriptions
        $qtdInscriptions = count($inscriptions);

        //Finding the students
        foreach($inscriptions as $inscription){
            $students[] = array('priority' => $inscription->priority, 'student' =>$inscription->user()->first());
        }

        //Sorting the student by the priority
        usort($students, $this->sortSubjectsByKeyAsc('priority'));

        return view('dashboard/searchEngine.show', [
            'subject' => $subject,
            'qtdInscriptions' => $qtdInscriptions,
            'position' => $position,
            ])->with('students', $students);
    }

    /*
    This is the search method. If the user choosed the optimized
    method of search, the optimizedSearch will be called
    */
    public function search(Request $request){
        //validation
        $this->validate($request, [
            'subject' => 'required'
        ]);
        
        $studentInscriptions = array();

        //Getting the institution from session
        $institution = Session::get('currentInstitution');

        //Finding the subject
        $subject = Subject::find($request->subject);

        //Getting the inscriptions of the subject on the spefied day
        $inscriptions = $subject->inscriptions()
            ->whereIn('status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
            ->get();
            
        $qtdInscriptions = count($inscriptions);

        //Type of search
        $searchType = 'conventional';

        //Getting the courses of the current institution
        $courses = $institution->courses()->get();

        //Getting the quantity o registered subjects.
        $qtdSubjects = count(DB::table('institutions')
        ->join('courses', 'institutions.id', '=', 'courses.institution_id')
        ->join('course_subject', 'courses.id', '=', 'course_subject.course_id')
        ->join('subjects', 'course_subject.subject_id', '=', 'subjects.id')
        ->where('institutions.id', '=', $institution->id)
        ->select('subjects.*')
        ->get());
        
        //Checking if there is inscriptions for that subject
        if($qtdInscriptions == 0){
            
            Session::flash('info_message', 'Não há inscrições para a disciplina '. $subject->name . '.');
            
            return view('dashboard/searchEngine.index', [
                'courses' => $courses,
                'searchType' => $searchType,
            ]);
        }
        
        //Getting the user's information for each inscriptions
        foreach($inscriptions as $inscription){
            $studentInscriptions[] = array('priority' => $inscription->priority, 'inscription' => $inscription,);
        }
      
        //Sorting the array of students by priority
        usort($studentInscriptions, $this->sortSubjectsByKeyAsc('priority'));
        
        return view('dashboard/searchEngine.index', [
            'courses' => $courses,
            'searchType' => $searchType,
            'qtdInscriptions' => $qtdInscriptions,
            'subject' => $subject,
            'studentInscriptions' => $studentInscriptions
        ]);

    }
    /*
    This function makes an optimized search that returns
    valuable information to help to create new DP Classes
    */
    public function optimizedSearch(Request $request){
    //Getting the institution from session
    $institution = Session::get('currentInstitution');

    $searchEngine = new SearchEngine();

    //Matrix with the subject's score and the subject
    $scoredSubjectsMon = array();
    $scoredSubjectsTue = array();
    $scoredSubjectsWed = array();
    $scoredSubjectsThu = array();
    $scoredSubjectsFri = array();
    $scoredSubjectsSat = array();

    //Getting subjects from the inscriptions for each day
    $subjectsInscriptionsMon = $this->getSubjectsFromInscriptionsByDay('Mon');
    $subjectsInscriptionsTue = $this->getSubjectsFromInscriptionsByDay('Tue');
    $subjectsInscriptionsWed = $this->getSubjectsFromInscriptionsByDay('Wed');
    $subjectsInscriptionsThu = $this->getSubjectsFromInscriptionsByDay('Thu');
    $subjectsInscriptionsFri = $this->getSubjectsFromInscriptionsByDay('Fri');
    $subjectsInscriptionsSat = $this->getSubjectsFromInscriptionsByDay('Sat');

    //For each subject of the following days, it will be calculated the subject's score
    //Monday
    foreach($subjectsInscriptionsMon as $subject){

        $summarySubject = new SummarySubject($subject->id, 'Mon');
        $score = $searchEngine->calcScore($summarySubject);

        //Array with [score, SummarySuject]
        $scoredSubjectsMon[] = array('score' => $score, 'summarySubject' => $summarySubject);
    }

    //Tuesday
    foreach($subjectsInscriptionsTue as $subject){

        $summarySubject = new SummarySubject($subject->id, 'Tue');
        $score = $searchEngine->calcScore($summarySubject);
        $scoredSubjectsTue[] = array('score' => $score, 'summarySubject' => $summarySubject);
    }

    //Wednesday
    foreach($subjectsInscriptionsWed as $subject){

        $summarySubject = new SummarySubject($subject->id, 'Wed');
        $score = $searchEngine->calcScore($summarySubject);
        $scoredSubjectsWed[] = array('score' => $score, 'summarySubject' => $summarySubject);
    }

    //Thursday
    foreach($subjectsInscriptionsThu as $subject){

        $summarySubject = new SummarySubject($subject->id, 'Thu');
        $score = $searchEngine->calcScore($summarySubject);
        $scoredSubjectsThu[] = array('score' => $score, 'summarySubject' => $summarySubject);
    }

    //Friday
    foreach($subjectsInscriptionsFri as $subject){

        $summarySubject = new SummarySubject($subject->id, 'Fri');
        $score = $searchEngine->calcScore($summarySubject);
        $scoredSubjectsFri[] = array('score' => $score, 'summarySubject' => $summarySubject);
    }

    //Saturday
    foreach($subjectsInscriptionsSat as $subject){

        $summarySubject = new SummarySubject($subject->id, 'Sat');
        $score = $searchEngine->calcScore($summarySubject);
        $scoredSubjectsSat[] = array('score' => $score, 'summarySubject' => $summarySubject);
    }

    //Sorting the scored subjects by the score value
    usort($scoredSubjectsMon, $this->sortSubjectsByKeyDesc('score'));
    usort($scoredSubjectsTue, $this->sortSubjectsByKeyDesc('score'));
    usort($scoredSubjectsWed, $this->sortSubjectsByKeyDesc('score'));
    usort($scoredSubjectsThu, $this->sortSubjectsByKeyDesc('score'));
    usort($scoredSubjectsFri, $this->sortSubjectsByKeyDesc('score'));
    usort($scoredSubjectsSat, $this->sortSubjectsByKeyDesc('score'));

    //For each result of inscriptions, it will be created a SummarySubject
    $searchType = 'optimized';

    //Getting the courses of the current institution
    $courses = $institution->courses()->get();

    //Getting the quantity o registered subjects.
    $qtdSubjects = count(DB::table('institutions')
    ->join('courses', 'institutions.id', '=', 'courses.institution_id')
    ->join('course_subject', 'courses.id', '=', 'course_subject.course_id')
    ->join('subjects', 'course_subject.subject_id', '=', 'subjects.id')
    ->where('institutions.id', '=', $institution->id)
    ->select('subjects.*')
    ->get());

    return view('dashboard/searchEngine.index', [
        'courses' => $courses,
        'searchType' => $searchType,
        'qtdSubjects' => $qtdSubjects
        ])->with('scoredSubjectsMon', $scoredSubjectsMon)
        ->with('scoredSubjectsTue', $scoredSubjectsTue)
        ->with('scoredSubjectsWed', $scoredSubjectsWed)
        ->with('scoredSubjectsThu', $scoredSubjectsThu)
        ->with('scoredSubjectsFri', $scoredSubjectsFri)
        ->with('scoredSubjectsSat', $scoredSubjectsSat);

    }

    /*
    This function returns the Subjcts from inscriptions on a specified day
    */
    private function getSubjectsFromInscriptionsByDay($day){
        //Getting the institution
        $institution = Session::get('currentInstitution');

        return $subjects = DB::table('institutions')
        ->join('courses', 'institutions.id', '=', 'courses.institution_id')
        ->join('course_subject', 'courses.id', '=', 'course_subject.course_id')
        ->join('subjects', 'course_subject.subject_id', '=', 'subjects.id')
        ->join('inscriptions', 'subjects.id', '=', 'inscriptions.subject_id')
        ->where('institutions.id', '=', $institution->id)
        ->whereIn('inscriptions.status', [Inscription::STATUS_OPEN, Inscription::STATUS_IN_PROGRESS])
        ->where('inscriptions.days', 'like', '%' . $day . '%')
        ->select('subjects.*')
        ->distinct()
        ->get();
    }

    /*
    This function will sort a given array in DESC order using the given key
    */
    private function sortSubjectsByKeyDesc($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($b[$key],$a[$key]);
        };
    }

    /*
    This function will sort a given array in ASC order using the given key
    */
    private function sortSubjectsByKeyAsc($key) {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key],$b[$key]);
        };
    }

}
