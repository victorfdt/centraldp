<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\User;

class DashboardController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        //$this->middleware('role:1', ['only' => ['studentView']]);
        //$this->middleware('role:2', ['only' => ['adminView']]);
        //$this->middleware('role:3', ['only' => ['masterView']]);
    }

    /*
    public function studentIndex() {
        return view('dashboard/student.index');
    }

    public function adminIndex() {
        return view('dashboard/admin.index');
    }

    public function masterIndex() {
        return view('dashboard/master.index');
    }
     * */

    public function index() {

        $user = Auth::user();

        if($user->type == $user::ROLE_STUDENT){
            return redirect('/dashboard/student');
        } elseif($user->type == $user::ROLE_ADMIN) {
            return redirect('/dashboard/admin');
        } elseif($user->type == $user::ROLE_MASTER) {
            return redirect('/dashboard/master');
        }

    }

    /*
        Display the profile information of the logged user.
        It wil
    */
    public function profile(){

        //Getting the logged user
        $user = Auth::user();
        $layout;

        //Checking the type of the user to load the proper layout
        if($user->type == User::ROLE_STUDENT){
            $layout = "layouts/dashboard.student";
        } elseif($user->type == User::ROLE_ADMIN){
            $layout = "layouts/dashboard.admin";
        } elseif($user->type == User::ROLE_MASTER){
            $layout = "layouts/dashboard.master";
        }

       return view('dashboard/profile.index', [
            'user' => $user,
            'layout' => $layout
        ]);

    }
}
