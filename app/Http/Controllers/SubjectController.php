<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Institution;
use App\Course;
use App\Subject;
use Session;
use DB;

class SubjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        //Master role
        $this->middleware('role:3');

        //Checking if its selected the current institution
        $this->middleware('checkCurrentInstitution');
    }

    /**
     * Index page of the Subject
     *
     * @return void
     */
    public function index() {
        //Getting only the institutions that have at least one course
        $institution = Session::get('currentInstitution');
        
        $courses = $institution->courses()->get();

        return view('dashboard/subject.index', [
            'institution' => $institution,
            'courses' => $courses
        ]);
    }

    /**
     * Load the subject creation page
     *
     * @return void
     */
    public function create() {
        //Getting only the institutions that have at least one course
        $institution = Session::get('currentInstitution');

        $availableCourses = $institution->courses()->orderBy('name', 'asc')->get();

        return view('dashboard/subject.create', [
            'institution' => $institution,
            'availableCourses' => $availableCourses
        ]);
    }

    /**
      * Creates a new Subject
     */
    public function store(Request $request) {

        //Validation
        //TODO |unique:subjects - Validar disciplinas com o mesmo nome
        $this->validate($request, [
            'name' => 'required|max:100',
            'available' => 'required',
            'hours' => 'required|integer',
            'selectedCourses' => 'required',
            'req_num_ins' => 'required|numeric|min:1'
        ]);

        //Creating a new subject
        $subject = new Subject();
        $subject->name = $request->name;
        $subject->available = $request->available;
        $subject->hours = $request->hours;
        $subject->req_num_ins = $request->req_num_ins;

        //Saving here, because it is necessary to save to generate an ID
        $subject->save();

        //For each selected courses, it will be created a record at pivot table
        foreach($request->selectedCourses as $courseId){
            $subject->courses()->attach($courseId);
        }

        $subject->save();

        Session::flash('success_message', 'A disciplina '. $subject->name . ' foi criada com sucesso!');

        return redirect('/dashboard/subject');
    }

    /**
     * Search subjects by the given form
     *
     * @return void
     */
    public function search(Request $request) {
        $institution = $request->session()->get('currentInstitution');
        $selectedCourseId = $request->course;
        $subjectName = $request->subjectName;
        $subjects = [];
        $availableCourses;
        $hours = $request->hours;

        //If the value is -1, the user selected the "all courses" field
        if($selectedCourseId == -1){

            /*
            TODO
            $courses = $institution->courses()->get();

            foreach ($courses as $course) {
                dd($course->subjects()->select("subject_id")->get());
                array_push($subjects, $course->subjects()->get());
            }
            dd($subjects);

            */

        } else {

            $course = Course::find($selectedCourseId);
            //Searching the subjects using the form's information

            //Checking the hours field was filled up
            if($request->hours != null){
                $subjects = $course->subjects()
                            ->where('name', 'like', '%' . $subjectName .  '%')
                            ->where('hours', '=', $request->hours)
                            ->orderBy('name', 'asc')
                            ->get();
            } else {
                $subjects = $course->subjects()
                            ->where('name', 'like', '%' . $subjectName .  '%')
                            ->orderBy('name', 'asc')
                            ->get();
            }

        }

        //If it was not found many subject, it will be shown an information message to the user
        if(sizeof($subjects) <= 0 ){
            Session::flash('info_message', 'Não foram encontradas disciplinas que satisfaçam os requisitos informados.');
        }

        //Getting the courses of the current institution
        $courses = $institution->courses()->get();

        return view('dashboard/subject.index', [
            'subjects' => $subjects,
            'institution' => $institution,
            'subjectName' => $subjectName,
            'selectedCourseId' => $selectedCourseId,
            'hours' => $hours,
            'courses' => $courses
        ]);
    }

    /**
     * Load the edition page of the subject.
     *
     */
    public function edit($id) {
        //Getting the selected subject
        $subject = Subject::find($id);

        //Getting the courses of this subject
        $selectedCourses = $subject->courses()->orderBy('name', 'asc')->get();
        $selectedCoursesId = [];

        foreach($selectedCourses as $course){
            $selectedCoursesId[] = $course->id;
        }

        //Getting the available courses
        $institution = $selectedCourses->first()->institution->get()[0];

        $availableCourses = collect(DB::table('courses')
            ->where('institution_id', '=', $institution->id)
            ->whereNotIn('id', $selectedCoursesId)
            ->orderBy('name', 'asc')
            ->get());

        return view('dashboard/subject.edit', [
            'subject' => $subject,
            'selectedCourses' => $selectedCourses,
            'availableCourses' => $availableCourses,
            'institution' => $institution
        ]);

    }

    /**
     * This function updates the subject
     * @param $request
     * @param $id subject id
     *
     * @return void
     */
    public function update(Request $request, $id) {
        //Validation
        $this->validate($request, [
            'name' => 'required|max:100',
            'available' => 'required',
            'hours' => 'required|integer',
            'selectedCourses' => 'required',
            'req_num_ins' => 'required|numeric|min:1'
        ]);

        //Finding the subject
        $subject = Subject::find($id);

        //Setting the new information
        $subject->name = $request->name;
        $subject->available = $request->available;
        $subject->hours = $request->hours;
        $subject->req_num_ins = $request->req_num_ins;

        //Removing all the related courses from the subject.
        //The next step will add the new selected courses
        $subject->courses()->detach();

        //For each selected courses, it will be created a record at pivot table
        foreach($request->selectedCourses as $courseId){
            $subject->courses()->attach($courseId);
        }

        $subject->save();

        //Success message
        Session::flash('success_message', 'A disciplina ' . $subject->name .  ' foi alterada com sucesso!');

        return redirect("/dashboard/subject/");
    }

    /**
     * Remove a subject.
     *
     * @return void
     */
    public function destroy($id) {
        $subject = Subject::find($id);
        $subjectName = $subject->name;
        $subject->delete();

        Session::flash('success_message', 'A disciplina '. $subjectName . ' foi removida com sucesso!');

        return redirect('/dashboard/subject/');
    }
}
