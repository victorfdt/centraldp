<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Task;
use Illuminate\Http\Request;

Route::auth();
//Route::resource('task', 'TaskController');
//Route::get('/', 'TaskController@index');
//Route::post('/task/search', 'TaskController@search');

//HomeController
Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/contact', 'HomeController@contact');
Route::get('/access_denied', 'HomeController@accessDenied');

//DashboardController
Route::get('/dashboard', 'DashboardController@index');
Route::get('/dashboard/profile', 'DashboardController@profile');
Route::get('/dashboard/student', 'DashboardStudentController@index');
Route::get('/dashboard/admin', 'DashboardAdminController@index');
Route::get('/dashboard/master', 'DashboardMasterController@index');

//InstitutionController
Route::get('/dashboard/institution/editCurrentInstitution', 'InstitutionController@editCurrentInstitution');
Route::get('/dashboard/institution/setCurrentInstitution/', 'InstitutionController@setCurrentInstitution');
Route::get('/dashboard/institution', 'InstitutionController@index');
Route::get('/dashboard/institution/create', 'InstitutionController@create');
Route::post('/dashboard/institution', 'InstitutionController@store');
Route::get('/dashboard/institution/{id}/edit', 'InstitutionController@edit');
Route::delete('/dashboard/institution/{id}', 'InstitutionController@destroy');
Route::get('/dashboard/institution/{id}', 'InstitutionController@update');

//CourseController
Route::get('/dashboard/course/', 'CourseController@index');
Route::get('/dashboard/course/create', 'CourseController@create');
Route::post('/dashboard/course', 'CourseController@store');
Route::get('/dashboard/course/{id}/edit', 'CourseController@edit');
Route::delete('/dashboard/course/{id}', 'CourseController@destroy');
Route::get('/dashboard/course/{id}', 'CourseController@update');

//SubjectController
Route::get('/dashboard/subject', 'SubjectController@index');
Route::get('/dashboard/subject/create', 'SubjectController@create');
Route::post('/dashboard/subject', 'SubjectController@store');
Route::post('/dashboard/subject/search', 'SubjectController@search');
Route::get('/dashboard/subject/{id}/edit', 'SubjectController@edit');
Route::get('/dashboard/subject/{id}', 'SubjectController@update');
Route::delete('/dashboard/subject/{id}', 'SubjectController@destroy');

//InscriptionController
Route::get('/dashboard/inscription/priority', 'InscriptionController@priority');
Route::get('/dashboard/inscription/priorityUpdate', 'InscriptionController@priorityUpdate');
Route::get('/dashboard/inscription', 'InscriptionController@index');
Route::get('/dashboard/inscription/create', 'InscriptionController@create');
Route::post('/dashboard/inscription', 'InscriptionController@store');
Route::get('/dashboard/inscription/{id}/edit', 'InscriptionController@edit');
Route::get('/dashboard/inscription/{id}', 'InscriptionController@update');
Route::delete('/dashboard/inscription/{id}', 'InscriptionController@destroy');

//SearchEngineController
Route::get('/dashboard/searchEngine/', 'SearchEngineController@index');
Route::get('/dashboard/searchEngine/search', 'SearchEngineController@search');
Route::get('/dashboard/searchEngine/optimized_search', 'SearchEngineController@optimizedSearch');
Route::get('/dashboard/searchEngine/show/{position}/{subjectId}/{day}', 'SearchEngineController@show');

//DPClassController
Route::get('/dashboard/dpclass/view_class/', 'DPClassController@viewClass');
Route::get('/dashboard/dpclass/', 'DPClassController@index');
Route::get('/dashboard/dpclass/search', 'DPClassController@search');
Route::get('/dashboard/dpclass/create', 'DPClassController@create');
Route::post('/dashboard/dpclass', 'DPClassController@store');
Route::get('/dashboard/dpclass/{id}/edit', 'DPClassController@edit');
Route::get('/dashboard/dpclass/{id}', 'DPClassController@update');
Route::delete('/dashboard/dpclass/{id}', 'DPClassController@destroy');


//AJAX
Route::post('/dashboard/getCoursesByInstituion/{id}', 'InscriptionController@ajaxGetCoursesByInstitution');
Route::post('/dashboard/getSubjectsByCourse/{id}', 'InscriptionController@ajaxGetSubjectsByCourse');
Route::post('/dashboard/getStudentsBySubject/{id}', 'DPClassController@ajaxGetStudentsBySubject');
Route::post('/dashboard/getStudentById/{id}', 'DPClassController@ajaxGetStudentById');

/*
Route::get('/aluno', ['middleware' => 'role:2', function () {
    echo "Aluno";
}]);
*/
