<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckCurrentInstitutionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $institution = Session::get('currentInstitution');
        
        //If there is not a current institution, the user will be sent to the selection page
        if($institution == null){
        Session::flash('info_message', 'Para acessar a area
        desejada, você deve primeiro selecionar uma insituição.');
        
            return redirect()->action('InstitutionController@editCurrentInstitution');
        }
        
        return $next($request);
    }
}
