<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'period'
    ];
    
    public function institution(){
        return $this->belongsTo('App\Institution');
    }
    
    /**
     * The subjects that belong to the course.
     */
    public function subjects()
    {
        return $this->belongsToMany('App\Subject')->withTimestamps();
    }
}
