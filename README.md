As a final Computer Engineering course assignment, I developed an administrative system called
CentralDP, which is focused on solving many issues regarding the creation of classes for students
who failed on a subject.

This software is able to group students from different courses which have the same subject
dependency and advise the university how to organize the classes based on the priority selected by
the students.

Prior coding, the development of this project required a deep understanding of the university and
students’ needs.

Technologies:
PHP (Laravel);
jQuery and jQuery UI;
Bootstrap;
MySQL;
Ubuntu OS (Developed on the cloud environment C9.io);