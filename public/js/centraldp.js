/**
    This function sets the modal content of the courses' delete button
*/
function deleteCourse(button){
    //Setting modal information
    $('#myModalTitle').text('Remover curso');
    $('#myModalBody').html("<p>Você deseja remover o curso selecionado?</p>");
    $('#myModalConfirm').html('Remover');

    $('#myModalConfirm').click(function(){
        $(button).closest('form').submit();
    });

    $('#myModal').modal();
}

/**
    This function sets the modal content of the institution' delete button
*/
function deleteInstitution(button){
    //Setting modal information
    $('#myModalTitle').text('Remover instituição');
    $('#myModalBody').html("<p><b>Ao remover uma instituição, todas os cursos relacionados a ela serão removidos também!</b></p><p>Você deseja remover a instituição selecionada?</p>");
    $('#myModalConfirm').html('Remover');

    $('#myModalConfirm').click(function(){
        $(button).closest('form').submit();
    });

    $('#myModal').modal();
}

/**
    This function sets the modal content of the institution' delete button
*/
function deleteSubject(button){
    //Setting modal information
    $('#myModalTitle').text('Remover disciplina');
    $('#myModalBody').html("<p>Você deseja remover a disciplina selecionada?</p>");
    $('#myModalConfirm').html('Remover');

    $('#myModalConfirm').click(function(){
        $(button).closest('form').submit();
    });

    $('#myModal').modal();
}

/**
    This function sets the modal content of the institution' delete button
*/
function deleteDPClass(button){
    //Setting modal information
    $('#myModalTitle').text('Remover turma de DP');
    $('#myModalBody').html("<p>Você deseja remover a turma de DP selecionada?</p>");
    $('#myModalConfirm').html('Remover');

    $('#myModalConfirm').click(function(){
        $(button).closest('form').submit();
    });

    $('#myModal').modal();
}

/**
    This function sets the modal content of the institution' delete button
*/
function deleteInscription(button){
    //Setting modal information
    $('#myModalTitle').text('Remover inscrição');
    $('#myModalBody').html("<p>Você deseja remover a inscrição selecionada?</p>");
    $('#myModalConfirm').html('Remover');

    $('#myModalConfirm').click(function(){
        $(button).closest('form').submit();
    });

    $('#myModal').modal();
}

/*This function this will select on the courses list the selected course chose before the search operation */
function getOldCourseValue(selectedCourseId){
    if(selectedCourseId != ""){
        $('#course > option').each(function(){
           if(this.value == selectedCourseId) {
               $(this).attr("selected", "selected");
           }
        });
    }
}

/**
  This function will load the functionality of move the course between the combobox:
  availableCourses e selectedCourses
 */
function loadSubjectButtonsNavigation(){
  $('#availableCourses').dblclick(function(){
      $('#availableCourses option:selected').appendTo($('#selectedCourses'));
  });

  $('#selectedCourses').dblclick(function(){
      $('#selectedCourses option:selected').appendTo($('#availableCourses'));
  });

  //When the addCourse button is clicked, the selected course is moved to the selected courses
  $('#addCourse').click(function(){
      $('#availableCourses option:selected').appendTo($('#selectedCourses'));
  });

  //When the removeCourse button is clicked, the selected course is moved to the available courses
  $('#removeCourse').click(function(){
      $('#selectedCourses option:selected').appendTo($('#availableCourses'));
  });
}

/*
    Setting up de datepicker plugin to the date format used in Brazil on the period dates
*/
function addDatePicketOnPeriodDates(){

    $("#start_date, #end_date").datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
}
