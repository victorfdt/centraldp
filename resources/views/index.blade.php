@extends('layouts.home')

@section('content')
<!-- Marketing Icons Section -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Bem Vindo ao CentralDP
            </h1>
        </div>
        <div class="col-lg-12">
            <p>
                O CentralDP tem como objetivo ajudar os alunos a finalizar seus estudos de maneira inteligente,
                propondo-se a montar turmas de disciplinas em regime de dependência de forma prática e rápida.
            </p>
        </div>
    
    </div>
@endsection