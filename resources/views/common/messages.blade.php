@if(Session::has('info_message'))
    <div class="alert alert-info" role="alert">{{ Session::get('info_message') }}</div>
@endif

@if(Session::has('success_message'))
    <div class="alert alert-success" role="alert">{{ Session::get('success_message') }}</div>
@endif

@if(Session::has('error_message'))
    <div class="alert alert-danger" role="alert">{{ Session::get('error_message') }}</div>
@endif

@if (count($errors) > 0)
<!-- Form Error List -->
<div class="alert alert-danger">
    <strong>Ops! Ocorreu algum erro</strong>

    <br><br>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif