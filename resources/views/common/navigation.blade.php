<script>
    $(function () {
        $('[data-toggle="popover"]').popover();
    })
</script>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">CentralDP</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ url('/about') }}">Sobre</a>
                </li>
                <li>
                    <a href="{{ url('/contact') }}">Contato</a>
                </li>

                @if( !Auth::check() )
                    <!-- Login -->
                    <form id="nav_login" action="{{ url('/login') }}" method="POST" class="navbar-form navbar-right">
                        {{ csrf_field() }}

                      <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <input type="text" id="email" name="email" class="form-control" placeholder="Email">
                      </div>

                      <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                      </div>

                      <input type="submit" class="btn btn-success" value="Entrar">
                      <a class="btn btn-primary" href="{{url('/register')}}" role="button">Cadastro</a>
                    </form>

                @else
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ url('dashboard/') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{url('/dashboard/profile/')}}"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>
                        <!--
                          <li>
                              <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                          </li>
                          <li>
                              <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                          </li>
                        -->
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url('/logout')}}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                @endif
                <!--

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Portfolio <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="portfolio-1-col.html">1 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-2-col.html">2 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-3-col.html">3 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-4-col.html">4 Column Portfolio</a>
                        </li>
                        <li>
                            <a href="portfolio-item.html">Single Portfolio Item</a>
                        </li>
                    </ul>
                </li>
                !-->
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
