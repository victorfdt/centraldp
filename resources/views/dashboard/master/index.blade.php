@extends('layouts/dashboard.master')

@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
           Dashboard <small> Olá {{ Auth::user()->name }}, bem vindo!</small>
        </h1>
        <!-- 
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Dashboard - MASTER!!
            </li>
        </ol>
        -->
    </div>
</div>
<!-- /.row -->
   
@endsection