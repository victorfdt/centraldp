<div class="row">
    <div class="col-lg-10">
        <h3>Resultado</h3>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        <form class="form-horizontal">
            <!-- QTD INSCRIPTIONS -->
            <div class="form-group">
                <label class="col-lg-3 control-label">Quantidade de inscritos</label>
                <p class="form-control-static col-lg-6">{{ $qtdInscriptions }} / {{ $subject->req_num_ins }}</p>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-lg-10">
        <h4>Alunos inscritos</h4>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
            <th>Nome</th>
            <th>Prioridade na disciplina</th>
            <th>Registro Acadêmico (RA)</th>
            <th>E-mail</th>
            <th>Dias</th>
        </thead>

        <!-- Table Body -->
        <tbody>
            @foreach ($studentInscriptions as $inscription)
            <tr>
                <!-- User name -->
                <td class="table-text">
                    <div>{{ $inscription['inscription']->user()->first()->name }} {{ $inscription['inscription']->user()->first()->surname }}</div>
                </td>

                <!-- Priority -->
                <td class="table-text">
                    <div>
                        {{ $inscription['priority'] }}
                    </div>
                </td>

                <!-- RA -->
                <td class="table-text">
                    <div>{{ $inscription['inscription']->user()->first()->ra }}</div>
                </td>

                <!-- EMAIL -->
                <td class="table-text">
                    <div>{{ $inscription['inscription']->user()->first()->email }}</div>
                </td>
                
                <!-- DAYS -->
                <td class="table-text">
                    <div>
                        @foreach ($inscription['inscription']->daysToStringArray() as $day)
                           <span class="label label-default">{{ $day }}</span> 
                        @endforeach
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>