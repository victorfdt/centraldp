@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Visualizar inscrições <small></small>
        </h1>
    </div>
</div>
<!-- /.row -->

<script>
$(function() {

    /*This function makes an AJAX request when the Course field is changed.
      It will search for all subjects of the selected course
    */
    function courseOnChange(){
        var token = "{{ csrf_token() }}";
        var courseId = $('#course').val();
        var url = "{{ url('/dashboard/getSubjectsByCourse/') }}" + "/" + courseId;

        $('#availableSubjects').empty();

        $.post( url, { _token: token })
        .done(function( data ){
            //sets the given JSON courses at the course select box
            $.each(data.subjects, function(i, obj) {
                $('#availableSubjects').append(
                    $('<option></option').val(obj.id).html(obj.name)
                );
            });
        });
    }

    courseOnChange();

    $('#course').change(function(){
        courseOnChange();
    });

    //Sets the search method
    $('#classDPForm').attr('action', "{{ url('/dashboard/searchEngine/search') }}");

    //Setting up de datepicker plugin to the date format used in Brazil
    //addDatePicketOnPeriodDates();

    //Checking the type of search
    if("{{ $searchType }}" == 'optimized'){
        optimizedSearchClick();
    }else{
        conventionalSearchClick();
    }

    //Managing method search buttons
        //Conventional Search
    $('#conventionalSearch').click(function(){
        conventionalSearchClick();
    });

    //Optimized Search
    $('#optimizedSearch').click(function(){
        optimizedSearchClick();
    });

    //Adding the pressed behavior to the group buttons used on the search method
    $(".btn-group > .btn").click(function(){
        $(this).addClass("active").siblings().removeClass("active");
    });

});

function optimizedSearchClick(){
    //Displays the optimized method and hide the conventional one
    $('#optimizedSearchMethod').show();
    $('#optimizedSearchResult').show();
    $('#conventionalSearchMethod').hide();
    $('#conventionalSearchResult').hide();
    $('#optimizedSearch').addClass("active").siblings().removeClass("active");

    //Sets the search method
    $('#classDPForm').attr('action', "{{ url('/dashboard/searchEngine/optimized_search') }}");
}

function conventionalSearchClick(){
    //Displays the conventional method and hide the optimized one
    $('#conventionalSearchMethod').show();
    $('#conventionalSearchResult').show();
    $('#optimizedSearchMethod').hide();
    $('#optimizedSearchResult').hide();
    $('#conventionalSearch').addClass("active").siblings().removeClass("active");

    //Sets the search method
    $('#classDPForm').attr('action', "{{ url('/dashboard/searchEngine/search') }}");
}
</script>

<!-- Display Messages -->
@include('common.messages')

<div class="row">
    <div class="col-lg-10">
        <p>Use os campos de busca abaixo para encontrar informações referentes às inscrições dos alunos.</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        <form action="/dashboard/searchEngine/search" class="form-horizontal" id="classDPForm">
            {{ csrf_field() }}

            <!-- SEARCH METHOD -->
            <div class="form-group {{ $errors->has('period') ? ' has-error' : '' }}">
                <label for="institution" class="col-lg-3 control-label">Método de buscar</label>
                <div class="col-lg-6">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" id="conventionalSearch" class="btn btn-default active">Convencional</button>
                        <button type="button" id="optimizedSearch" class="btn btn-default">Busca otimizada</button>
                    </div>
                </div>
            </div>

            <!-- CONVENTIONAL SEARCH METHOD -->
            <div id="conventionalSearchMethod">
                <!-- COURSE -->
                <div class="form-group">
                    <label for="course" class="col-lg-3 control-label">
                        Curso
                    </label>
                    <div class="col-lg-6">
                        <select name="course" class="form-control" id="course" value="{!! old('course') !!}">
                            @foreach ($courses as $course)
                                <option value="{{ $course->id }}">{{ $course->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <!-- SUBJECT -->
                <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                    <label for="curso" class="col-lg-3 control-label">
                        Disciplina
                    </label>
                    <div class="col-lg-6">
                        <select name="subject" class="form-control" id="availableSubjects" value="{!! old('subject') !!}">
                        </select>
                    </div>
                    <div class="col-lg-1">
                        <button type="button" onclick="$('#classDPForm').submit();" class="btn btn-primary">Buscar  <i class="fa fa-search"></i></button>
                    </div>
                </div>

                <!-- PERIOD
                <div class="form-group {{ $errors->has('date_period') ? ' has-error' : '' }}">
                    <label for="start_date" class="col-lg-3 control-label">Período</label>
                    <div class="col-lg-6">
                        <input id="start_date" class="form-control input-date-period" type="text"
                            name="start_date"
                            placeholder="Data início"/>
                        <input id="end_date" class="form-control input-date-period" type="text"
                            name="end_date"
                            placeholder="Data fim"/>
                    </div>
                    <div class="col-lg-1">
                        <button type="button" onclick="$('#classDPForm').submit();" class="btn btn-primary">Buscar  <i class="fa fa-search"></i></button>
                    </div>
                </div>
                -->
            </div>

            <!-- OPTIMIZED SEARCH -->
            <div id="optimizedSearchMethod">
                <div class="form-group">
                    <label for="start_date" class="col-lg-3 control-label">Descrição</label>
                    <p class="form-control-static col-lg-6">Este  método de busca utiliza um algoritmo de ordenação que exibe
                    as melhores disciplinas para cada dia da semana.</p>
                    <div class="col-lg-1">
                        <button type="button" onclick="$('#classDPForm').submit();" class="btn btn-primary">Buscar  <i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- OPTIMIZED SEARCH RESULT TABLE -->
<div id="optimizedSearchResult">
    <!-- Monday -->
    @if(isset($scoredSubjectsMon) && count($scoredSubjectsMon) > 0)
            @php ($dayName = 'Segunda-feira')
            @php ($dayValue = 'Mon')
            @php ($scoredSubjects = $scoredSubjectsMon)
            @include('dashboard/searchEngine.optimized_search_result')
    @endif

    <!-- Tuesday -->
    @if(isset($scoredSubjectsTue) && count($scoredSubjectsTue) > 0)
            @php ($dayName = 'Terça-feira')
            @php ($dayValue = 'Tue')
            @php ($scoredSubjects = $scoredSubjectsTue)
            @include('dashboard/searchEngine.optimized_search_result')
    @endif

    <!-- Wednesday -->
    @if(isset($scoredSubjectsWed) && count($scoredSubjectsWed) > 0)
            @php ($dayName = 'Quarta-feira')
            @php ($dayValue = 'Wed')
            @php ($scoredSubjects = $scoredSubjectsWed)
            @include('dashboard/searchEngine.optimized_search_result')
    @endif

    <!-- Thursday -->
    @if(isset($scoredSubjectsThu) && count($scoredSubjectsThu) > 0)
            @php ($dayName = 'Quinta-feira')
            @php ($dayValue = 'Thu')
            @php ($scoredSubjects = $scoredSubjectsThu)
            @include('dashboard/searchEngine.optimized_search_result')
    @endif

    <!-- Friday -->
    @if(isset($scoredSubjectsFri) && count($scoredSubjectsFri) > 0)
            @php ($dayName = 'Sexta-feira')
            @php ($dayValue = 'Fri')
            @php ($scoredSubjects = $scoredSubjectsFri)
            @include('dashboard/searchEngine.optimized_search_result')
    @endif

    <!-- Saturday -->
    @if(isset($scoredSubjectsSat) && count($scoredSubjectsSat) > 0)
            @php ($dayName = 'Sábado')
            @php ($dayValue = 'Sat')
            @php ($scoredSubjects = $scoredSubjectsSat)
            @include('dashboard/searchEngine.optimized_search_result')
    @endif
</div>

<!-- CONVENTIONAL SEARCH RESULT TABLE -->
<div id="conventionalSearchResult">
    @if(isset($studentInscriptions) && count($studentInscriptions) > 0)
        @include('dashboard/searchEngine.search_result')
    @endif
</div>

@endsection
