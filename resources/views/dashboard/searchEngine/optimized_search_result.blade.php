<div class="row">
        <div class="col-lg-12">
            <h3>{{ $dayName }}</h3>
            <div class="table-responsive">
                <table class="table table-hover">

                    <!-- Table Headings -->
                    <thead>
                        <th class="col-lg-1">Posição</th>
                        <th class="col-lg-2">Nome da disciplina</th>
                        <th class="col-lg-2">Qtd. inscritos / Qtd. mínima</th>
                        <th class="col-lg-1">Informações</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @php($i = 1)
                        @foreach ($scoredSubjects as $subject)
                            <!-- If there is the minimum quantity of inscriptions, the style will change to info -->
                            @if($subject['summarySubject']->getQtdInscriptions() >= $subject['summarySubject']->getSubject()->req_num_ins)
                                <tr class="info">
                            @else
                                <tr>
                            @endif

                            <!-- Score -->
                                <td class="col-lg-1">
                                    <div>{{ $i }}</div>
                                </td>

                                <!-- Subject Name -->
                                <td class="col-lg-2">
                                    <div>{{ $subject['summarySubject']->getSubject()->name }}</div>
                                </td>

                                <!-- Quantity of inscriptions -->
                                <td class="col-lg-2">
                                    <div>
                                        {{ $subject['summarySubject']->getQtdInscriptions() }} /
                                        {{ $subject['summarySubject']->getSubject()->req_num_ins }}
                                    </div>
                                </td>

                                <td class="col-lg-1">
                                    <a class="btn btn-info"
                                        href="{{ url('/dashboard/searchEngine/show/' . $i . '/' . $subject['summarySubject']->getSubject()->id . '/' . $dayValue ) }}">
                                        <i class="fa fa-info"></i>
                                    </a>
                                </td>
                            </tr>
                            <!-- Incrementing  -->
                            @php($i++)
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
