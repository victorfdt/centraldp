@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Visualizar inscrições <small>Informações</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-10">
        <p>Abaixo estão as informações coletadas referentes à disciplina {{ $subject->name }}</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        <form class="form-horizontal">
            <!-- SUBJECT -->
            <div class="form-group">
                <label class="col-lg-3 control-label">Disciplina</label>
                <p class="form-control-static col-lg-6">{{ $subject->name }}</p>
            </div>

            <!-- POSITION -->
            <div class="form-group">
                <label class="col-lg-3 control-label">Posição na tabela</label>
                <p class="form-control-static col-lg-6">{{ $position }}</p>
            </div>

            <!-- QTD INSCRIPTIONS -->
            <div class="form-group">
                <label class="col-lg-3 control-label">Quantidade de inscritos</label>
                <p class="form-control-static col-lg-6">{{ $qtdInscriptions }} / {{ $subject->req_num_ins }}</p>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-lg-10">
        <h3>Alunos inscritos</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
            <th>Nome</th>
            <th>Prioridade na disciplina</th>
            <th>Registro Acadêmico (RA)</th>
            <th>E-mail</th>
        </thead>

        <!-- Table Body -->
        <tbody>
            @foreach ($students as $student)
            <tr>
                <!-- NAME -->
                <td class="table-text">
                    <div>{{ $student['student']->name }} {{ $student['student']->surname }}</div>
                </td>

                <!-- PRIORITY -->
                <td class="table-text">
                    <div>
                        {{ $student['priority'] }}
                    </div>
                </td>

                <!-- RA -->
                <td class="table-text">
                    <div>{{ $student['student']->ra }}</div>
                </td>

                <!-- EMAIL -->
                <td class="table-text">
                    <div>{{ $student['student']->email }}</div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-lg-1">
        <a class="btn btn-primary" href="{{ url('/dashboard/searchEngine/optimized_search')}} " >
            Voltar
        </a>
    </div>
</div>
@endsection
