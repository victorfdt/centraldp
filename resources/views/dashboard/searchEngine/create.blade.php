@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Turmas de DP <small>Criar</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<script>
$(function() {

    /*This function makes an AJAX request when the Course field is changed.
      It will search for all subjects of the selected course
    */
    function courseOnChange(){
        var token = "{{ csrf_token() }}";
        var courseId = $('#course').val();
        var url = "{{ url('/dashboard/getSubjectsByCourse/') }}" + "/" + courseId;

        $('#availableSubjects').empty();

        $.post( url, { _token: token })
        .done(function( data ){
            //sets the given JSON courses at the course select box
            $.each(data.subjects, function(i, obj) {
                $('#availableSubjects').append(
                    $('<option></option').val(obj.id).html(obj.name)
                );
            });
        });
    }

    courseOnChange();

    $('#course').change(function(){
        courseOnChange();
    });

    $('#algorithmSearchMethod').hide();

    //Setting up de datepicker plugin to the date format used in Brazil
    addDatePicketOnPeriodDates();
});
</script>

<!-- Display Messages -->
@include('common.messages')

<!-- Display Modal -->
@include('common.modal')

<div class="row">
    <div class="col-lg-10">
        <p>Preencha o formulário abaixo para criar uma Turma de DP.</p>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
         <h3>Dados da Turma</h3>
    </div>
</div>
    
<!-- Form -->
<form id="dpClassForm" class="form-horizontal" method="POST" action="{{ url('/dashboard/searchEngine') }}">
    <div class="row">
        {{ csrf_field() }}
        <div class="col-lg-8">
            <!-- COURSE -->
            <div class="form-group">
                <label for="course" class="col-lg-3 control-label">
                    Curso
                </label>
                <div class="col-lg-8">
                    <select name="course" class="form-control" id="course" value="{!! old('course') !!}">
                        @foreach ($courses as $course)
                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
    
            <!-- SUBJECT -->
            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-3 control-label">
                    Disciplina
                </label>
                <div class="col-lg-8">
                    <select name="subject" class="form-control" id="availableSubjects" value="{!! old('subject') !!}">
                    </select>
                </div>
            </div>
                
            <!-- PROFESSOR'S NAME -->
            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-3 control-label">
                    Nome do professor
                </label>
                <div class="col-lg-8">
                    <input name="professorName" placeholder="Nome" class="form-control" value="{!! old('professorName') !!}"></input>
                </div>
            </div>
                
            <!-- PROFESSOR'S EMAIL -->
            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-3 control-label">
                    E-mail do professor
                </label>
                <div class="col-lg-8">
                    <input name="professorEmail" placeholder="E-mail" class="form-control" value="{!! old('professorName') !!}"></input>
                </div>
            </div>
            
            <!-- PERIOD -->
            <div class="form-group {{ $errors->has('date_period') ? ' has-error' : '' }}">
                <label for="start_date" class="col-lg-3 control-label">Período</label>
                <div class="col-lg-9">
                    <input id="start_date" class="form-control input-date-period" type="text"
                        name="start_date"
                        placeholder="Data início"/>
                    <input id="end_date" class="form-control input-date-period" type="text"
                        name="end_date"
                        placeholder="Data fim"/>
                </div>
            </div>
        </div>
        
        <div class="col-lg-4">
               <!-- Days -->
            <div class="form-group {{ $errors->has('days') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-3 control-label">
                     Dias
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Selecione ao menos um dia para cursar a disciplina."></i>
                 </label>

                <div class="col-lg-5">
                    <div class="checkbox">
                        <label><input type="checkbox" name="monday">Segunda-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="tuesday" >Terça-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="wednesday" >Quarta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="thursday" >Quinta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="friday" >Sexta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="saturday" >Sábado</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
             <h3>Seleção de alunos</h3>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8">
            <!-- STUDENT -->
            <label for="institution" class="col-lg-3 control-label">Buscar aluno por</label>
            <div class="col-lg-6">
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" id="searchByName" class="btn btn-default active">Nome</button>
                    <button type="button" id="searchByRA" class="btn btn-default">Ra</button>
                    <button type="button" id="searchByEmail" class="btn btn-default">E-mail</button>
                </div>
                
                </br></br>
                
                <input type="text" class="form-control" name="searchStudent" id="searchStudent" placeholder="Campo de busca" />
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-8">
            <!-- STUDENT -->
            <label for="institution" class="col-lg-3 control-label">Alunos selecionados</label>
            <div class="col-lg-6">
                <div class="table-responsive">
                    <table class="table table-hover">
                
                        <!-- Table Headings -->
                        <thead>
                        <th>Nome</th>
                        <th>RA</th>
                        <th>E-mail</th>
                        <th>Remover</th>
                        </thead>
                
                        <!-- Table Body -->
                        <tbody>
                            <tr>
                                <!-- Student Name -->
                                <td class="table-text">
                                    <div>Victor</div>
                                </td>
                
                                <!-- RA -->
                                <td class="table-text">
                                    <div>
                                        1511031172
                                    </div>
                                </td>
                
                                <!-- EMAIL -->
                                <td class="table-text">
                                    <div>victordt@yahoo.com.br</div>
                                </td>
                                
                                <!-- Delete Button -->
                                <td>
                                    <button type="button" class="btn btn-danger" onclick="deleteSubject(this);">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <select name="availableCourses" size="5" multiple class="form-control coursesList" id="availableCourses">
                    <option>Victor Forato Di Trani - 1510031172 - victordt@yahoo.com.br</option>
                </select>
            </div>
        </div>
    </div>
</form>

<!-- BUTTONS -->
<div class="row">
    <!-- Return button -->
    <div class="col-lg-1">
        <a href="{{ url('/dashboard/searchEngine') }}" class="btn btn-primary">Voltar</a>
    </div>
    
    <!--  -->
    <div class="col-lg-11 text-right">
        <button type="button" onclick="$('#dpClassForm').submit();"  class="btn btn-success">Criar</button>
    </div>
</div>
@endsection
