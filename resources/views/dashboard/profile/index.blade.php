@extends($layout)
@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Perfil <small> Dados cadastrais</small>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p>Abaixo encontram-se seus dados cadastrais. Para alterá-los, clique no botão "Alterar dados e senha".</p>
    </div>
</div>


<form class="form-horizontal">

    <!-- INSTITUTION -->
    @if(isset($user->institution))
      <div class="form-group">
          <label class="col-lg-2 control-label">Instituição</label>
          <p class="form-control-static col-lg-8">{{ $user->institution->name }}</p>
      </div>
    @endif

    <!-- NAME -->
    <div class="form-group">
        <label class="col-lg-2 control-label">Nome</label>
        <p class="form-control-static col-lg-8">{{ $user->name }}</p>
    </div>

    <!-- SURNAME -->
    @if($user->surname != "")
        <div class="form-group">
            <label class="col-lg-2 control-label">Sobrenome</label>
            <p class="form-control-static col-lg-8">{{ $user->surname }}</p>
        </div>
    @endif


      <!-- RA -->
      @if($user->ra != "")
        <div class="form-group">
            <label class="col-lg-2 control-label">Registro Acadêmico (RA)</label>
            <p class="form-control-static col-lg-8">{{ $user->ra }}</p>
        </div>
      @endif

    <!-- EMAIL -->
    <div class="form-group">
        <label class="col-lg-2 control-label">E-mail</label>
        <p class="form-control-static col-lg-8">{{ $user->email }}</p>
    </div>

    <!-- BUTTONS -->
    <div class="form-group">
        <div class="col-lg-1 text-left">
            <a class="btn btn-primary" href="{{ url('/dashboard')}}" >
                Voltar
            </a>
        </div>
        <div class="col-lg-11 text-right">
            <a class="btn btn-success" href="#" >
                Alterar dados e senha
            </a>
        </div>
    </div>

</form>


<!-- /.row -->

@endsection
