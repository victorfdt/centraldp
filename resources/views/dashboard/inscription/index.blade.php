@extends('layouts/dashboard.student')

@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Inscrição de disciplina<small></small>
        </h1>       
    </div>       
</div>
<!-- /.row -->

<!-- Display Messages -->
@include('common.messages')

<!-- Display Modal -->
@include('common.modal')

<!-- Table of Institutions-->
@if (isset($inscriptions) && count($inscriptions) > 0)
<!-- Table with database results -->
<div class="table-responsive">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
            <th>Prioridade</th>
            <th>Disciplina</th>
            <th>Situação</th>
            <th>Dias</th>
            <th>Editar</th>
            <th>Remover</th>
        </thead>

        <!-- Table Body -->
        <tbody id="inscriptionsTable">
            @foreach ($inscriptions as $inscription)
            <tr>
                <!-- Priority -->
                <td class="table-text">
                    <div><b>{{ $inscription->priority }}</b></div>
                </td>
                
                <!-- Subject -->
                <td class="table-text">
                    <div>{{ $inscription->subject->name }}</div>
                </td>
                
                <!-- Status -->
                <td class="table-text">
                    <div>
                        {{ $inscription->getStatusDescription() }}
                    </div>
                </td>
                
                <!-- Dias -->
                <td class="table-text">
                    <div>
                        @foreach ($inscription->daysToStringArray() as $day)
                           <span class="label label-default">{{ $day }}</span> 
                        @endforeach
                    </div>
                </td>

                <!-- Update Button -->
                <td>
                    <form action="{{ url('dashboard/inscription/'.$inscription->id . '/edit') }}" method="GET">
                        {{ csrf_field() }}
                        {{ method_field('') }}

                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </form>
                </td>

                <!-- Delete Button -->
                <td>
                    <form action="{{ url('dashboard/inscription/'.$inscription->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="button" class="btn btn-danger" onclick="deleteInscription(this);">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
<!-- Add button -->
<div class="row">   
    <div class="col-lg-12">
        <p>Você não possui nenhuma inscrição.</p>
    </div>
</div>
@endif

<!-- BUTTONS -->
<div class="row">   
    <div class="col-lg-12 text-right">
        <a class="btn btn-success" role="button" href="{{ url('/dashboard/inscription/priority')}} " >
            Alterar prioridade
        </a>
        
        <a class="btn btn-success" role="button" href="{{ url('/dashboard/inscription/create')}} " >
            Criar inscrição
        </a>
    </div>
</div>
@endsection