@extends('layouts/dashboard.student')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Inscrição de disciplina <small>Criar</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<!-- Display Validation Errors -->
@include('common.messages')

<script>
$(function() {

    /*This function makes an AJAX request when the Course field is changed.
      It will search for all subjects of the selected course
    */
    function courseOnChange(){
        var token = "{{ csrf_token() }}";
        var courseId = $('#course').val();
        var url = "{{ url('/dashboard/getSubjectsByCourse/') }}" + "/" + courseId;

        $('#availableSubjects').empty();

        $.post( url, { _token: token })
        .done(function( data ){
            //sets the given JSON courses at the course select box
            $.each(data.subjects, function(i, obj) {
                $('#availableSubjects').append(
                    $('<option></option').val(obj.id).html(obj.name)
                );
            });
        });
    }

    courseOnChange();

    $('#course').change(function(){
        courseOnChange();
    });

});
</script>

<!-- Checking is there is available subjects -->
@if(!$hasSubject)
<div class="row">
    <div class="col-lg-12 text-left">
        <a class="btn btn-primary" href="{{ url('/dashboard/inscription')}} " >
            Voltar
        </a>
    </div>
</div>
@else
<!-- Table of Institutions-->
<div class="row">
    <div class="col-lg-12">
        <p>Preencha as informações abaixo para criar uma inscrição na disciplina.</p>
        {!! Form::open(['url' => '/dashboard/inscription', 'class' => 'form-horizontal']) !!}
            {{ csrf_field() }}

            <!-- INSTITUTION -->
            <div class="form-group">
                <label for="institution" class="col-lg-2 control-label">Instituição</label>
                <p class="form-control-static col-lg-8">{{ $institutionName }}</p>
            </div>

            <!-- Available Courses -->
            <div class="form-group">
                <label for="course" class="col-lg-2 control-label">
                    Curso
                </label>
                <div class="col-lg-3">
                    <select name="course" class="form-control" id="course" value="{!! old('course') !!}">
                        @foreach ($courses as $course)
                            <option value="{{ $course->id }}">{{ $course->name }} - @if($course->period == 1)Manhã @else Noite  @endif</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- Subjects -->
            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    Disciplina
                </label>
                <div class="col-lg-3">
                    <select name="subject" class="form-control" id="availableSubjects" value="{!! old('subject') !!}">
                    </select>
                </div>
            </div>

            <!-- Days -->
            <div class="form-group {{ $errors->has('days') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                     Dias
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Selecione ao menos um dia para cursar a disciplina."></i>
                 </label>

                <div class="col-lg-2">
                    <div class="checkbox">
                        <label><input type="checkbox" name="monday">Segunda-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="tuesday" >Terça-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="wednesday" >Quarta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="thursday" >Quinta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="friday" >Sexta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="saturday" >Sábado</label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-1 text-left">
                    <a class="btn btn-primary" href="{{ url('/dashboard/inscription')}} " >
                        Voltar
                    </a>
                </div>
                <div class="col-lg-11 text-right">
                    <button type="submit" class="btn btn-success">Criar</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>

</div>
@endif

@endsection
