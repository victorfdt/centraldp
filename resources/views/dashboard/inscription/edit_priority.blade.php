@extends('layouts/dashboard.student')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Inscrição de disciplina <small>Alterar prioridade</small>
        </h1>       
    </div>       
</div>
<!-- /.row -->

<script>
$(function() {
    $('#inscriptionsTable').sortable();
});

function submitButton(){
        
    var inscriptionsId = [];
    
    //Getting the inscriptions id and putting on the array by the order
    $('#inscriptionsTable input').each(function(){
        inscriptionsId.push(this.value);
    });
    
    //Setting the array on the input that will be send to the controller.
    $('#inscriptionsOrder').val(inscriptionsId);
    
    //Submit the form
    $('#updatePriorityForm').submit();
}
</script>

<!-- Display Validation Errors -->
@include('common.errors')

<!-- Display Messages -->
@include('common.messages')

<!-- Description of the page -->
<div class="row">
    <div class="col-lg-12">
        <p>A prioridade representa o nível de interesse na disciplina, sendo que disciplinas nas primeiras posições têm maior
        importância que as localizadas no fim da lista. </p>
        <p>Para alterar a prioridade da disciplina, arraste a disciplina para baixo ou para cima de acordo com a posição desejada.</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-2">
        <!-- Table of Institutions-->
        <div class="table-responsive">
            <table class="table table-hover">
        
                <!-- Table Headings -->
                <thead>
                    <th>Prioridade</th>
                </thead>
        
                <!-- Table Body -->
                <tbody>
                    @for ($i = 1; $i <= count($inscriptions); $i++)
                        <tr>
                            <td>
                                <b>{{ $i }}</b>
                            </td>
                        </tr>
                    @endfor
                </tbody>
            </table>
        </div>

    </div>
    <div class="col-lg-10">
        <!-- Table of Institutions-->
        <div class="table-responsive">
            <table class="table table-hover">
        
                <!-- Table Headings -->
                <thead>
                    <th class="col-lg-9">Disciplina</th>
                </thead>
        
                <!-- Table Body -->
                <tbody id="inscriptionsTable">
                    @foreach ($inscriptions as $inscription)
                    <tr>
                        <!-- Subject -->
                        <td class="table-text info">
                            <div>
                                <span class="glyphicon glyphicon-move" aria-hidden="true"></span>
                                {{ $inscription->subject->name }}
                            </div>
                            <input type="hidden" name="inscriptionId" value="{{ $inscription->id }}"/>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- Add button -->
<div class="row">   
    <div class="col-lg-6 text-left">
        <a class="btn btn-primary" role="button" href="{{ url('/dashboard/inscription/')}} " >
            Voltar
        </a>
    </div>
    <div class="col-lg-6 text-right">
        <button type="button" class="btn btn-success" onclick="submitButton();">Salvar</button>
    </div>
</div>

<!-- -->
<form id="updatePriorityForm" class="form-horizontal" action="{{ url('/dashboard/inscription/priorityUpdate') }}">
    <input type="hidden" id="inscriptionsOrder" name="inscriptionsOrder"/>
</form>

@endsection