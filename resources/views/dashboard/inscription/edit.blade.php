@extends('layouts/dashboard.student')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Inscrição de disciplina <small>Editar</small>
        </h1>       
    </div>       
</div>
<!-- /.row -->

<script>
$(function() {
    
    //Setting the priority of the inscription.
    $('#priority > option').each(function(){
       if(this.value == "{{ $inscription->priority }}") {
           $(this).attr("selected", "selected");
       }
    });
});
</script>

<!-- Display Validation Errors -->
@include('common.errors')

<!-- Display Messages -->
@include('common.messages')

<!-- Table of Institutions-->
<div class="row">
    <div class="col-lg-12">
        <form id="updateInscriptionForm" class="form-horizontal" action="{{ url('/dashboard/inscription/' . $inscription->id) }}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            
            <!-- INSTITUTION 
            <div class="form-group">
                <label for="institution" class="col-lg-2 control-label">Instituição</label>
                <p class="form-control-static col-lg-8">{{ $institutionName }}</p>
            </div>
            
            -->
            
            <!-- SUBJECT -->
            <div class="form-group">
                <label for="subject" class="col-lg-2 control-label">Disciplina</label>
                <p class="form-control-static col-lg-8">{{ $subjectName }}</p>
            </div>
            
            <!-- PRIORITY 
            <div class="form-group">
                <label for="subject" class="col-lg-2 control-label">Prioridade</label>
                <p class="form-control-static col-lg-8">{{ $inscription->priority }}</p>
            </div>
            
            -->
            
            <!-- Days -->
            <div class="form-group {{ $errors->has('days') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                     Dias
                    <i class="fa fa-question-circle" data-toggle="tooltip" 
                        data-placement="top" title="Selecione ao menos um dia para cursar a disciplina."></i>
                 </label>
                <div class="col-lg-3">
                    <div class="checkbox">
                        <label><input type="checkbox" name="monday" {{ $daysArray['monday'] === true ? "checked='checked'" : "" }}>Segunda-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="tuesday" {{ $daysArray['tuesday'] === true ? "checked='checked'" : "" }}>Terça-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="wednesday" {{ $daysArray['wednesday'] === true ? "checked='checked'" : "" }}>Quarta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="thursday" {{ $daysArray['thursday'] === true ? "checked='checked'" : "" }}>Quinta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="friday" {{ $daysArray['friday'] === true ? "checked='checked'" : "" }}>Sexta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="saturday" {{ $daysArray['saturday'] === true ? "checked='checked'" : "" }}>Sábado</label>
                    </div>
                </div>
                
                
            </div>
            
            <div class="form-group">
                <div class="col-lg-1 text-left">
                    <a class="btn btn-primary" href="{{ url('/dashboard/inscription')}} " >
                        Voltar
                    </a>
                </div>
                <div class="col-lg-11 text-right">
                    <button type="submit" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection