@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Disciplinas <small>Criar</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<!-- Display Validation Errors -->
@include('common.errors')

<script>
$(function() {
    loadSubjectButtonsNavigation();
});
</script>

<!-- Display Validation Errors -->
@include('common.flash')
<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        <form class="form-horizontal" method="POST" action="{{ url('/dashboard/subject') }}">
            {{ csrf_field() }}

            <!-- NAME -->
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-lg-2 control-label">Nome</label>
                <div class="col-lg-3">
                    <input type="text" name="name" value="{!! old('name') !!}" class="form-control" id="subjectName" placeholder="Nome"/>
                </div>
            </div>

            <!-- INSTITUTION -->
            <div class="form-group {{ $errors->has('period') ? ' has-error' : '' }}">
                <label for="institution" class="col-lg-2 control-label">Instituição</label>
                <p class="form-control-static col-lg-8">{{ $institution->name }}</p>
            </div>

            <!-- AVAILABLE -->
            <div class="form-group {{ $errors->has('available') ? ' has-error' : '' }}">
                <label for="institution" class="col-lg-2 control-label">
                   Disponível
                   <i class="fa fa-question-circle" data-toggle="tooltip"
                       data-placement="top" title="Este campo permite ou não ao estudante selecionar a disciplina no momento da inscrição de disciplina."></i>
               </label>
                <div class="col-lg-3">
                    <label class="radio-inline"><input type="radio" value="1" checked name="available">Sim</label>
                    <label class="radio-inline"><input type="radio" value="0" name="available">Não</label>
                </div>
            </div>

            <!-- HOURS -->
            <div class="form-group {{ $errors->has('hours') ? ' has-error' : '' }}">
                <label for="hours" class="col-lg-2 control-label">Carga horária</label>
                <div class="col-lg-3">
                    <input type="text" name="hours" value="{!! old('hours') !!}" id="hours" class="form-control" placeholder="Horas"/>
                </div>
            </div>

            <!-- REQUIRED NUMBER OF INSCRIPTIONS -->
            <div class="form-group {{ $errors->has('req_num_ins') ? ' has-error' : '' }}">
                <label for="req_num_ins" class="col-lg-2 control-label">Quantidade requerida de alunos
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Quantidade de alunos necessários para a montagem da turma referente a disciplina que está sendo criada."></i>
                </label>
                <div class="col-lg-3">
                    <input type="text" name="req_num_ins" value="{!! old('req_num_ins') !!}" id="req_num_ins" class="form-control" placeholder="Número de alunos"/>
                </div>
            </div>

            <!-- COURSE -->
            <div class="form-group {{ $errors->has('selectedCourses') ? ' has-error' : '' }}">
                <!-- Available Courses -->
                <label for="curso" class="col-lg-2 control-label">
                    Curso disponíveis
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Estes são os cursos disponíveis para associar a disciplina que esta sendo criada. Use as setas direcionais para adicionar ou remover um ou mais cursos."></i>
                </label>
                <div class="col-lg-3">
                    <select name="availableCourses" size="5" multiple class="form-control coursesList" id="availableCourses">
                        @foreach ($availableCourses as $course)
                          <option value="{{ $course->id }}">{{ $course->name }} - @if($course->period == 1)Manhã @else Noite  @endif</option>
                        @endforeach
                    </select>
                </div>

                <!-- ACTION BUTTONS -->
                <div class="col-lg-1 text-right">
                    <button type="button" class="btn btn-default btn-md" id="addCourse">
                      <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                    </button>

                    <button type="button" class="btn btn-default btn-md" id="removeCourse">
                      <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                    </button>
                </div>

                 <!-- Selected Courses -->
                <label for="curso" class="col-lg-2 control-label">
                    Curso associados
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Estes são os cursos associados a disciplina que esta sendo criada. Use as setas direcionais para adicionar ou remover um ou mais cursos."></i>
                </label>
                <div class="col-lg-3">
                    <select name="selectedCourses[]" size="5" multiple class="form-control coursesList" id="selectedCourses">
                    </select>
                </div>
            </div>

            <!-- BUTTONS -->
            <div class="form-group">
                <div class="col-lg-1 text-left">
                    <a class="btn btn-primary" href="{{ url('/dashboard/subject')}} " >
                        Voltar
                    </a>
                </div>

                <div class="col-lg-11 text-right">
                    <button type="submit" class="btn btn-success">Criar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
