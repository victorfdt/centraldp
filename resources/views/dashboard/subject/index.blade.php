@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Disciplinas <small></small>
        </h1>
    </div>
</div>
<!-- /.row -->

<script>
$(function() {
    @if(isset($selectedCourseId))
        getOldCourseValue("{{$selectedCourseId}}");
    @endif
});
</script>

<!-- Display Messages -->
@include('common.messages')

<!-- Display Modal -->
@include('common.modal')

@if (isset($courses) && count($courses) > 0)
<div class="row">
    <div class="col-lg-10">
        <p>Use os campos abaixo para buscar disciplinas</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        {!! Form::open(['url' => '/dashboard/subject/search', 'class' => 'form-horizontal']) !!}
        {{ csrf_field() }}

        <!-- INSTITUTION -->
        <div class="form-group {{ $errors->has('period') ? ' has-error' : '' }}">
            <label for="institution" class="col-lg-3 control-label">Instituição</label>
            <p class="form-control-static col-lg-8">{{ $institution->name }}</p>
        </div>

        <!-- COURSE -->
        <div class="form-group {{ $errors->has('period') ? ' has-error' : '' }}">
            <label for="course" class="col-lg-3 control-label">Curso</label>
            <div class="col-lg-6">
                <select name="course" class="form-control" id="course">
                    @foreach ($courses as $course)
                      <option value="{{ $course->id }}">{{ $course->name }} - @if($course->period == 1)Manhã @else Noite  @endif</option>
                    @endforeach
                </select>
            </div>
        </div>

        <!-- NAME -->
        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-lg-3 control-label">Nome</label>
            <div class="col-lg-6">
                @if(isset($subjectName))
                {!! Form::text('subjectName', $subjectName, array('class' => 'form-control', 'placeholder' => 'Nome')) !!}
                @else
                {!! Form::text('subjectName', null, array('class' => 'form-control', 'placeholder' => 'Nome')) !!}
                @endif
            </div>
        </div>

        <!-- HOURS -->
        <div class="form-group {{ $errors->has('hours') ? ' has-error' : '' }}">
            <label for="hours" class="col-lg-3 control-label">Carga horária</label>
            <div class="col-lg-6">
                @if(isset($hours))
                {!! Form::text('hours', $hours, array('class' => 'form-control', 'placeholder' => 'Horas')) !!}
                @else
                {!! Form::text('hours', null, array('class' => 'form-control', 'placeholder' => 'Horas')) !!}
                @endif
            </div>
            <div class="col-lg-1">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<!-- ADD BUTTON -->
<div class="row">
    <div class="col-lg-12 text-right">
        <a class="btn btn-success" href="{{ url('/dashboard/subject/create')}} " >Criar disciplina</a>
    </div>
</div>
@else
<div class="row">
    <div class="col-lg-12">
        <p><b>Antes de criar uma disciplina, é necessário criar ao menos um curso.</b></p>
    </div>
</div>
@endif

<!-- RESULT TABLE -->
@if (isset($subjects) && count($subjects) > 0)
<div class="row">
    <div class="col-lg-10">
        <h3>Resultados</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
        <th>Nome</th>
        <th>Disponível</th>
        <th>Carga horária</th>
        <th>Quantidade requerida de alunos</th>
        <th>Editar</th>
        <th>Remover</th>
        </thead>

        <!-- Table Body -->
        <tbody>
            @foreach ($subjects as $subject)
            <tr>
                <!-- Subject Name -->
                <td class="table-text">
                    <div>{{ $subject->name }}</div>
                </td>

                <!-- Available -->
                <td class="table-text">
                    <div>
                        @if($subject->available == 1)
                            Sim
                        @else
                            Não
                        @endif
                    </div>
                </td>

                <!-- Hours -->
                <td class="table-text">
                    <div>{{ $subject->hours }}</div>
                </td>

                <!-- REQUIRED NUMBER OF INSCRIPTIONS -->
                <td class="table-text">
                    <div>{{ $subject->req_num_ins }}</div>
                </td>

                <!-- Update Button -->
                <td>
                    <form action="{{ url('dashboard/subject/'.$subject->id . '/edit') }}" method="GET">
                        {{ csrf_field() }}
                        {{ method_field('') }}

                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </form>
                </td>

                <!-- Delete Button -->
                <td>
                    <form action="{{ url('dashboard/subject/'.$subject->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="button" class="btn btn-danger" onclick="deleteSubject(this);">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection
