@extends('layouts/dashboard.student')

@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Dashboard <small> Olá {{ Auth::user()->name }}, bem vindo!</small>
        </h1>
        <!-- 
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Bem vindo ao Dashboard!
            </li>
        </ol>
        -->
    </div>
    
    <div class="col-lg-12">
        <p>Esta é sua área de inscrição e acompanhamento das disciplinas de DP.</p>
        <p>Utilize as opções disponíveis na aba <span class="fa fa-fw fa-university"></span><b>Acadêmico</b>, localizada no menu à esquerda, para se inscrever nas 
        disciplinas desejadas e também para acompanhar a situação das inscrições feitas.</p>
    </div>
</div>
<!-- /.row -->
   
@endsection