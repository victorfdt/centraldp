@extends('layouts/dashboard.master')

@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cursos <small>{{$institution->name}} - criar</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<!-- Display Validation Errors -->
@include('common.errors')

<!-- Table of Institutions-->
<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        <form class="form-horizontal" method="POST" action="{{ url('/dashboard/course') }}">
            {{ csrf_field() }}
            <!-- NAME -->
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-sm-2 control-label">Nome</label>
                <div class="col-lg-7">
                    <input type="text" name="name" class="form-control" id="courseName" placeholder="Nome"/>
                </div>
            </div>

            <!-- PERIOD -->
            <div class="form-group {{ $errors->has('period') ? ' has-error' : '' }}">
                <label for="name" class="col-sm-2 control-label">Período</label>
                <div class="col-lg-7">
                    <select name="period" class="form-control" id="coursePeriod">
                      <option value="1">Manhã</option>
                      <option value="2">Noite</option>
                    </select>
                </div>
            </div>

            <!-- INSTITUTION -->
            <input type="hidden" value="{{$institution->id}}" name="institution"/>

            <!-- BUTTONS -->
            <div class="form-group">
                <div class="col-lg-1 text-left">
                    <a class="btn btn-primary" href="{{ url('/dashboard/course/')}} " >
                        Voltar
                    </a>
                </div>
                <div class="col-lg-11 text-right">
                    <button type="submit" class="btn btn-success">Criar</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
