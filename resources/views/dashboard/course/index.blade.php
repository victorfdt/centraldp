@extends('layouts/dashboard.master')

@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cursos <small>{{Session::get('currentInstitution.name')}}</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<!-- Display Messages -->
@include('common.messages')

<!-- Display Modal -->
@include('common.modal')

<!-- Table of Courses-->
@if(isset($courses) && count($courses) > 0)
        <!-- Table with database results -->
        <div class="table-responsive">
            <table class="table table-hover">

                <!-- Table Headings -->
                <thead>
                    <th>Nome do curso</th>
                    <th>Período</th>
                    <th>Editar</th>
                    <th>Remover</th>
                </thead>

                <!-- Table Body -->
                <tbody>
                    @foreach ($courses as $course)
                    <tr>
                        <td class="table-text">
                            {{ $course->name }}
                        </td>

                    <!-- Period -->
                    <td class="table-text">
                        @if($course->period == 1)
                            Manhã
                        @elseif($course->period == 2)
                            Noite
                        @endif
                    </td>

                    <!-- Update Button -->
                    <td>
                        <form action="{{ url('dashboard/course/'.$course->id . '/edit') }}" method="GET">
                            {{ csrf_field() }}
                            {{ method_field('') }}

                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-pencil"></i>
                            </button>
                        </form>
                    </td>
                    <!-- Delete Button -->
                    <td>
                        <form action="{{ url('dashboard/course/'.$course->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="button" class="btn btn-danger" onclick="deleteCourse(this);">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
            <div class="row">
                <div class="col-lg-12">
                    <p>Não há cursos cadastrados. Clique no botão <a class="btn btn-success" role="button" href="{{ url('/dashboard/course/create')}} " >
                        Criar curso
                    </a></p>
                </div>
            </div>
        @endif

    <!-- Add button -->
    <div class="row">
        <div class="col-lg-6 text-left">
            <a class="btn btn-primary" role="button" href="{{ url('/dashboard/course')}} " >
                Voltar
            </a>
        </div>
        <div class="col-lg-6 text-right">
            <a class="btn btn-success" role="button" href="{{ url('/dashboard/course/create')}} " >
                Criar curso
            </a>
        </div>
    </div>
@endsection
