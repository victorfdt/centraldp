@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Instituição <small>Criar</small>
        </h1>       
    </div>       
</div>
<!-- /.row -->

<!-- Display Validation Errors -->
@include('common.errors')

<!-- Table of Institutions-->
<div class="row">
    <div class="col-lg-12">
        <form class="form-horizontal" method="POST" action="{{ url('/dashboard/institution') }}">
            {{ csrf_field() }}
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-sm-2 control-label">Nome</label>
                <div class="col-lg-7">
                    <input type="text" name="name" class="form-control" id="instituionName" placeholder="Nome"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-1 text-left">
                    <a class="btn btn-primary" href="{{ url('/dashboard/institution')}} " >
                        Voltar
                    </a>
                </div>
                <div class="col-lg-11 text-right">
                    <button type="submit" class="btn btn-success">Criar</button>
                </div>
            </div>
        </form>
    </div>

</div>

@endsection