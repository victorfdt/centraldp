@extends('layouts/dashboard.master')

@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Instituição <small></small>
        </h1>
    </div>
</div>
<!-- /.row -->

<!-- Display Messages -->
@include('common.messages')

<!-- Display Modal -->
@include('common.modal')

<!-- Table of Institutions-->
@if (count($institutions) > 0)
<!-- Table with database results -->
<div class="table-responsive">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
        <th>Nome</th>
        <th>Editar</th>
        <th>Remover</th>
        </thead>

        <!-- Table Body -->
        <tbody>
            @foreach ($institutions as $institution)
            <tr>
                <!-- Task Name -->
                <td class="table-text">
                    <div>{{ $institution->name }}</div>
                </td>

                <!-- Update Button -->
                <td>
                    <form action="{{ url('dashboard/institution/'.$institution->id . '/edit') }}" method="GET">
                        {{ csrf_field() }}
                        {{ method_field('') }}

                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </form>
                </td>

                <!-- Delete Button -->
                <td>
                    <form action="{{ url('dashboard/institution/'.$institution->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="button" class="btn btn-danger" onclick="deleteInstitution(this);">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

<!-- Add button -->
<div class="row">
    <div class="col-lg-12 text-right">
        <a class="btn btn-success" role="button" href="{{ url('/dashboard/institution/create')}} " >
            Criar instituição
        </a>
    </div>
</div>
@endsection
