@extends('layouts/dashboard.master')

@section('content')

<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Instituição <small>Selecionar instituição corrente</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<!-- Display Validation Errors -->
@include('common.errors')

<!-- Display Messages -->
@include('common.messages')

<script>
$(function() {

    //Setting on the available institution the current institution from the session
    var currentInstitutionId = "{{ Session::get('currentInstitution.id') }}";
    if(currentInstitutionId != ""){
        $('#institution > option').each(function(){
           if(this.value == currentInstitutionId) {
               $(this).attr("selected", "selected");
           }
        });
    }

});
</script>

<!-- Table of Institutions-->
@if(isset($institutions) && count($institutions) > 0)
<div class="row">
    <div class="col-lg-12">
        <p>Escolha uma das instituições disponíveis para definí-la como corrente</p>
        <p>Essa informação é utilizada em outras telas do sistema para buscar por cursos e disciplinas.</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12" >
        <form class="form-horizontal" action="{{ url('/dashboard/institution/setCurrentInstitution/') }}">
            {{ csrf_field() }}

            <!-- INSTITUTION -->
            <div class="form-group {{ $errors->has('period') ? ' has-error' : '' }}">
                <label for="institution" class="col-lg-2 control-label">Instituição</label>
                <div class="col-lg-3">
                    <select name="institution" class="form-control" id="institution">
                        @foreach ($institutions as $institution)
                          <option value="{{ $institution->id }}">{{ $institution->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-1">
                    <a class="btn btn-primary" href="{{ url('/dashboard/institution') }} " >
                        Voltar
                    </a>
                </div>
                <div class="col-lg-11 text-right">
                    <button type="submit" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@else
<div class="row">
    <div class="col-lg-12">
        <p><b>É necessário criar ao menos uma instituição para que seja possível selecioná-la como corrente.</b></p>
    </div>
</div>
@endif

@endsection
