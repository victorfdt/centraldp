@extends('layouts/dashboard.student')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Turmas de DP <small></small>
        </h1>
    </div>
</div>

<!-- Display Messages -->
@include('common.messages')

<div class="row">
    <div class="col-lg-10">
        <p>Abaixo encontram-se as Turmas de DP que você está registrado.</p>
    </div>
</div>
<!-- RESULT TABLE -->
@if (isset($dpClasses) && count($dpClasses) > 0)
<div class="table-responsive">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
        <th>Disciplina</th>
        <th>Status</th>
        <th>Nome do professor</th>
        <th>Email do professor</th>
        <th>Dias</th>
        <th class="col-lg-2">Descrição</th>
        </thead>

        <!-- Table Body -->
        <tbody>
            @foreach ($dpClasses as $dpClass)
            <tr>
                <!-- Subject Name -->
                <td class="table-text">
                    <div>{{ $dpClass->subject()->name }}</div>
                </td>

                <!-- Status -->
                <td class="table-text">
                    <div>
                        {{ $dpClass->getStatusDescription() }}
                    </div>
                </td>
                
                <!-- Professor Name -->
                <td class="table-text">
                    <div>{{ $dpClass->professor_name }}</div>
                </td>
                
                <!-- Professor Name -->
                <td class="table-text">
                    <div>{{ $dpClass->professor_email }}</div>
                </td>
                
                <!-- Days -->
                <td class="table-text">
                    <div>
                        @foreach ($dpClass->daysToStringArray() as $day)
                           <span class="label label-default">{{ $day }}</span> 
                        @endforeach
                    </div>
                </td>
                
                <!-- Description -->
                <td class="table-text">
                    <div><i class="fa fa-question-circle" data-toggle="tooltip"
                       data-placement="top" title="{{ $dpClass->description }}"></i></div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection
