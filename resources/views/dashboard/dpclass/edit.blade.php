@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Turmas de DP <small> Editar</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<script>
$(function() {
    //Adding the pressed behavior to the group buttons used on the search method
    $(".btn-group > .btn").click(function(){
        $(this).addClass("active").siblings().removeClass("active");
    });
    
    //This code select all the options of the selected Student field.
    //It is necessary because the information will only be
    //send on the request if the option be selected.
    $('#submitButton').click(function () {
        console.log("APERTOU");
        $('#selectedStudents option').prop('selected', true);
        $('#availableStudents option').prop('selected', true);
        $('#classDPForm').submit();
    });
    
    //loadNewStudents();
    
    @if(isset($dpClass))
        setStatus("{{ $dpClass->status }}");
    @endif
});

/*This function makes an AJAX request when the Course field is changed.
      It will search for all subjects of the selected course
*/
function loadNewStudents(){
    var token = "{{ csrf_token() }}";
    var subjectId = "{{ $dpClass->subject_id }}";
    var url = "{{ url('/dashboard/getStudentsBySubject/') }}" + "/" + subjectId;

    //Cleaning the select
    $('#availableStudents').empty();
    
    //Cleaning the student table
    $('#availableStudentsTableBody').html("");
    
    $.post( url, { _token: token })
    .done(function( data ){
        //sets the given JSON courses at the course select box
        $.each(data.students, function(i, student) {
            
            //Adding the students on the hidden select
            $('#availableStudents').append(
                $('<option></option').val(student['student'].id)
            );
            
            //Adding the students on the table
             $('#availableStudentsTableBody').append(
                "<tr> <td>" + student['student'].name + " " + student['student'].surname + "</td> <td>" + student['student'].ra + "</td> <td>" + student['student'].email + "</td>" + "<td>Adicionar</td> </tr>"
            );
        });
    });
}

function removeStudent(studentId){
    alert(studentId);
}

/*This function this will select on the status list the status of the current DPClass */
function setStatus(status){
    if(status != ""){
        $('#status > option').each(function(){
           if(this.value == status) {
               $(this).attr("selected", "selected");
           }
        });
    }
}

/*
    This function removes a student from the current DPClass
*/
function addStudent(element, userId){
    //Getting all the line of the selected student
    studentLine = $(element).closest("tr");
    
    var token = "{{ csrf_token() }}";
    var url = "{{ url('/dashboard/getStudentById/') }}" + "/" + userId;
    
    $.post( url, { _token: token })
    .done(function( data ){
        //sets the given JSON courses at the course select box
        student = data.student;
            
        //Adding the students on the hidden select
        $('#selectedStudents').append(
            $('<option></option').val(student.id)
        );
        
        //Removing the id of the user from the select
        $('#availableStudents > option').each(function(){
           if(this.value == userId) {
               $(this).remove();
           }
        });
        
        //Adding the students on the table
         $('#studentTableBody').append(
            "<tr> <td>" + student.name + " " + student.surname + "</td> <td>" + student.ra + "</td> <td>" + student.email + "</td>" + "<td>" +  "<button type='button' class='btn btn-danger' onclick='removeStudent(this," + student.id + ");'><i class='fa fa-trash'></i></button></td></tr>"
        );
        
    });
    
    $(element).closest("tr").remove();
}

function removeStudent(element, userId){
    
    //Getting all the line of the selected student
    studentLine = $(element).closest("tr");
    
    var token = "{{ csrf_token() }}";
    var url = "{{ url('/dashboard/getStudentById/') }}" + "/" + userId;
    
    $.post( url, { _token: token })
    .done(function( data ){
        //sets the given JSON courses at the course select box
        student = data.student;
            
        //Adding the students on the hidden select
        $('#availableStudents').append(
            $('<option></option').val(student.id)
        );
        
        //Removing the id of the user from the select
        $('#selectedStudents > option').each(function(){
           if(this.value == userId) {
               $(this).remove();
           }
        });
        
        //Adding the students on the table
         $('#availableStudentsTableBody').append(
            "<tr> <td>" + student.name + " " + student.surname + "</td> <td>" + student.ra + "</td> <td>" + student.email + "</td>" + "<td>" +  "<button type='button' class='btn btn-primary' onclick='addStudent(this," + student.id + ");'><i class='fa fa-plus'></i></button></td></tr>"
        );
        
    });
    
    $(element).closest("tr").remove();
}

</script>

<!-- Display Messages -->
@include('common.messages')

<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        <form action="{{ url('/dashboard/dpclass/' . $dpClass->id) }}" class="form-horizontal" id="classDPForm">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <!-- SUBJECT -->
            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                <label for="subject" class="col-lg-2 control-label">
                    Disciplina
                </label>
                <p class="form-control-static col-lg-6">{{ $dpClass->subject()->name }}</p>
            </div>
            
            <!-- PROFESSOR NAME -->
            <div class="form-group {{ $errors->has('professorName') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    Nome do professor
                </label>
                <div class="col-lg-6">
                    <input type="text" name="professorName" class="form-control" 
                        value="{{ $dpClass->professor_name }}" placeholder="Nome do professor"/>
                </div>
            </div>
            
            <!-- PROFESSOR EMAIL -->
            <div class="form-group {{ $errors->has('professorEmail') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    E-mail do professor
                </label>
                <div class="col-lg-6">
                    <input type="text" name="professorEmail" class="form-control" 
                        value="{{ $dpClass->professor_email }}" placeholder="E-mail do professor"/>
                </div>
            </div>
            
            <!-- STATUS -->
            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    E-mail do professor
                </label>
                <div class="col-lg-6">
                    <select name="status" class="form-control" id="status">
                        <option value="1">Aguardando documentação</option>
                        <option value="2">Em andamento</option>
                        <option value="3">Encerrada</option>
                    </select>
                </div>
            </div>
            
            <!-- Days -->
            <div class="form-group {{ $errors->has('days') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                     Dias
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Selecione os dias da semana que está disciplina será ministrada."></i>
                 </label>

                <div class="col-lg-6">
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Mon" {{ $daysArray['monday'] === true ? "checked='checked'" : "" }}>Segunda-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Tue" {{ $daysArray['tuesday'] === true ? "checked='checked'" : "" }}>Terça-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Wed" {{ $daysArray['wednesday'] === true ? "checked='checked'" : "" }}>Quarta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Thu" {{ $daysArray['thursday'] === true ? "checked='checked'" : "" }}>Quinta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Fri" {{ $daysArray['friday'] === true ? "checked='checked'" : "" }}>Sexta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Sat" {{ $daysArray['saturday'] === true ? "checked='checked'" : "" }}>Sábado</label>
                    </div>
                </div>
            </div>
            
            
            <!-- DESCRIPTION -->
            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    Descrição
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Descreva informações importantes, como por exemplo os horários dos dias que disicplina será ministrada e o número da sala de aula."></i>
                </label>
                <div class="col-lg-6">
                    <textarea name="description" id="description" class="form-control" rows="5"
                         placeholder="Informações importantes">{{ $dpClass->description }}</textarea>
                </div>
            </div>
            
            <!-- AVAILABLE STUDENTS -->
            <!-- This select will store the id of the assigned students -->
            <select name="availableStudents[]" multiple class="form-control" id="availableStudents" style="display: none;">
                @foreach($availableStudents as $user)
                    <option value="{{ $user->id }}"></option>
                @endforeach
            </select>
            
            <!-- SELECTED STUDENTS -->
            <!-- This select will store the id of the assigned students -->
            <select name="selectedStudents[]" multiple class="form-control" id="selectedStudents" style="display: none;">
                @foreach($students as $user)
                    <option value="{{ $user->id }}"></option>
                @endforeach
            </select>
        </form>
    </div>
</div>

<!-- AVAILABLE STUDENTS -->
<div class="row">
    <div class="col-lg-10">
        <h3>Alunos disponíveis</h3>
    </div>
</div>

<!-- RESULT TABLE -->
<div class="table-responsive" id="availableStudentsTable">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
            <th class="col-lg-3">Nome</th>
            <th class="col-lg-3">Registro Acadêmico (RA)</th>
            <th class="col-lg-3">E-mail</th>
            <th class="col-lg-1">Adicionar</th>
        </thead>
        <!-- Table Body -->
        <tbody id="availableStudentsTableBody">
            @foreach ($availableStudents as $student)
            <tr>
                <!-- Student Name -->
                <td class="table-text">
                    <div>{{ $student->name }} {{ $student->surname }}</div>
                </td>

                <!-- Student RA -->
                <td class="table-text">
                    <div>{{ $student->ra }}</div>
                </td>

                <!-- Student Email -->
                <td class="table-text">
                    <div>{{ $student->email }}</div>
                </td>
                
                <!-- Add Button -->
                <td class="table-text">
                    <button type="submit" class="btn btn-primary" onclick="addStudent(this, {{ $student->id }});">
                        <i class="fa fa-plus"></i>
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-lg-10">
        <h3>Alunos inscritos</h3>
    </div>
</div>

<!-- CURRENT STUDENTS -->

<!-- SEARCH METHOD
<form class="form-horizontal" id="classDPForm">
    <div class="form-group">
        <label for="institution" class="col-lg-3 control-label">Método de buscar do aluno</label>
        <div class="col-lg-6">
            <div class="btn-group" role="group" aria-label="...">
                <button type="button" id="conventionalSearch" class="btn btn-default active">Nome</button>
                <button type="button" id="optimizedSearch" class="btn btn-default">E-mail</button>
                <button type="button" id="optimizedSearch" class="btn btn-default">Registro Acadêmico (RA)</button>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-lg-3 control-label"></label>
        <div class="col-lg-6">
            <input type="text" name="professorName" class="form-control" 
                value="{!! old('studentName') !!}" placeholder="Nome do aluno"/>
        </div>
    </div>
</form>
-->

<!-- RESULT TABLE -->
<div class="table-responsive" id="studentTable">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
            <th class="col-lg-3">Nome</th>
            <th class="col-lg-3">Registro Acadêmico (RA)</th>
            <th class="col-lg-3">E-mail</th>
            <th class="col-lg-1">Remover</th>
        </thead>
        <!-- Table Body -->
        <tbody id="studentTableBody">
            @foreach ($students as $student)
            <tr>
                <!-- Student Name -->
                <td class="table-text">
                    <div>{{ $student->name }} {{ $student->surname }}</div>
                </td>

                <!-- Student RA -->
                <td class="table-text">
                    <div>{{ $student->ra }}</div>
                </td>

                <!-- Student Email -->
                <td class="table-text">
                    <div>{{ $student->email }}</div>
                </td>
                
                <!-- Delete Button -->
                <td class="table-text">
                    <button type="button" class="btn btn-danger" onclick="removeStudent(this, {{ $student->id }});">
                        <i class="fa fa-trash"></i>
                    </button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-lg-1 text-left">
        <a class="btn btn-primary" href="{{ url('/dashboard/dpclass/') }}">Voltar</a>
    </div>
    <div class="col-lg-11 text-right">
        <button class="btn btn-success" id="submitButton">Salvar</button>
    </div>
</div>
@endsection
