@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Turmas de DP <small></small>
        </h1>
    </div>
</div>
<!-- /.row -->

<script>
$(function() {

    /*This function makes an AJAX request when the Course field is changed.
      It will search for all subjects of the selected course
    */
    function courseOnChange(){
        var token = "{{ csrf_token() }}";
        var courseId = $('#course').val();
        var url = "{{ url('/dashboard/getSubjectsByCourse/') }}" + "/" + courseId;

        $('#availableSubjects').empty();

        $('#availableSubjects').append("<option value='-1'>Todos as disciplinas</option>");

        $.post( url, { _token: token })
        .done(function( data ){
            //sets the given JSON courses at the course select box
            $.each(data.subjects, function(i, obj) {
                $('#availableSubjects').append(
                    $('<option></option').val(obj.id).html(obj.name)
                );
            });
        });
    }

    courseOnChange();

    $('#course').change(function(){
        courseOnChange();
    });
});
</script>

<!-- Display Messages -->
@include('common.messages')

<!-- Display Modal -->
@include('common.modal')

<div class="row">
    <div class="col-lg-10">
        <p>Use os campos de busca abaixo para encontrar informações referentes as turmas de DP cadastradas.</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        {!! Form::open(['url' => '/dashboard/dpclass/search', 'method' => 'GET', 'class' => 'form-horizontal', 'id' => 'classDPForm']) !!}
            {{ csrf_field() }}
            <!-- COURSE -->
            <div class="form-group">
                <label for="course" class="col-lg-3 control-label">
                    Curso
                </label>
                <div class="col-lg-6">
                    <select name="course" class="form-control" id="course" value="{!! old('course') !!}">
                        <option value="-1">Todos os cursos</option>
                        @foreach ($courses as $course)
                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- SUBJECT -->
            <div class="form-group">
                <label for="curso" class="col-lg-3 control-label">
                    Disciplina
                </label>
                <div class="col-lg-6">
                    <select name="subject" class="form-control" id="availableSubjects" value="{!! old('subject') !!}">
                        <option value="-1">Todos as disciplinas</option>
                    </select>
                </div>
            </div>

            <!-- PROFESSOR NAME -->
            <div class="form-group">
                <label for="curso" class="col-lg-3 control-label">
                    Nome do professor
                </label>
                <div class="col-lg-6">
                    @if(isset($professorName))
                        {!! Form::text('professorName', $professorName, array('class' => 'form-control', 'placeholder' => 'Nome do professor')) !!}
                    @else
                        {!! Form::text('professorName', null, array('class' => 'form-control', 'placeholder' => 'Nome do professor')) !!}
                    @endif
                </div>
            </div>

            <!-- STUDENT NAME -->
            <div class="form-group">
                <label for="curso" class="col-lg-3 control-label">
                    Nome do aluno
                </label>
                <div class="col-lg-6">
                    @if(isset($studentName))
                        {!! Form::text('studentName', $studentName, array('class' => 'form-control', 'placeholder' => 'Nome do aluno')) !!}
                    @else
                        {!! Form::text('studentName', null, array('class' => 'form-control', 'placeholder' => 'Nome do aluno')) !!}
                    @endif
                </div>
                <div class="col-lg-1">
                    <button type="button" onclick="$('#classDPForm').submit();" class="btn btn-primary">Buscar  <i class="fa fa-search"></i></button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>

<div class="row">
    <div class="col-lg-12 text-right">
        <a class="btn btn-success" href="{{ url('/dashboard/dpclass/create') }}">Criar turma</a>
    </div>
</div>

<!-- RESULT TABLE -->
@if (isset($dpclasses) && count($dpclasses) > 0)
<div class="row">
    <div class="col-lg-10">
        <h3>Resultados</h3>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
        <th>Disciplina</th>
        <th>Status</th>
        <th>Quantidade alunos</th>
        <th>Nome do professor</th>
        <th>Editar</th>
        <th>Remover</th>
        </thead>

        <!-- Table Body -->
        <tbody>
            @foreach ($dpclasses as $dpclass)
            <tr>
                <!-- Subject Name -->
                <td class="table-text">
                    <div>{{ $dpclass->subject()->name }}</div>
                </td>

                <!-- Status -->
                <td class="table-text">
                    <div>
                        {{ $dpclass->getStatusDescription() }}
                    </div>
                </td>

                <!-- Quantity of Users -->
                <td class="table-text">
                    <div>{{ $dpclass->users()->count() }}</div>
                </td>

                <!-- Professor Name -->
                <td class="table-text">
                    <div>{{ $dpclass->professor_name }}</div>
                </td>

                <!-- Update Button -->
                <td>
                    <form action="{{ url('dashboard/dpclass/'.$dpclass->id . '/edit') }}" method="GET">
                        {{ csrf_field() }}
                        {{ method_field('') }}

                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-pencil"></i>
                        </button>
                    </form>
                </td>

                <!-- Delete Button -->
                <td>
                    <form action="{{ url('dashboard/dpclass/'.$dpclass->id) }}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="button" class="btn btn-danger" onclick="deleteDPClass(this);">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif
@endsection
