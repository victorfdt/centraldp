@extends('layouts/dashboard.master')

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Turmas de DP <small> Criar</small>
        </h1>
    </div>
</div>
<!-- /.row -->

<script>
$(function() {

    courseOnChange();

    $('#course').change(function(){
        courseOnChange();
    });
    
    $('#subject').change(function(){
        subjectOnChange();
    });
    
    //Adding the pressed behavior to the group buttons used on the search method
    $(".btn-group > .btn").click(function(){
        $(this).addClass("active").siblings().removeClass("active");
    });
    
    //This code select all the options of the selected Student field.
    //It is necessary because the information will only be
    //send on the request if the option be selected.
    $('#submitButton').click(function () {
        $('#selectedStudents option').prop('selected', true);
        $('#classDPForm').submit();
    });

});

/*This function makes an AJAX request when the Course field is changed.
      It will search for all subjects of the selected course
    */
    function courseOnChange(){
        var token = "{{ csrf_token() }}";
        var courseId = $('#course').val();
        var url = "{{ url('/dashboard/getSubjectsByCourse/') }}" + "/" + courseId;

        $('#subject').empty();
        
        $.post( url, { _token: token })
        .done(function( data ){
            //sets the given JSON courses at the course select box
            $.each(data.subjects, function(i, obj) {
                $('#subject').append(
                    $('<option></option').val(obj.id).html(obj.name)
                );
            });
            subjectOnChange();
        });
    }
    
    /*This function makes an AJAX request when the Course field is changed.
      It will search for all subjects of the selected course
    */
    function subjectOnChange(){
        var token = "{{ csrf_token() }}";
        var subjectId = $('#subject').val();
        var url = "{{ url('/dashboard/getStudentsBySubject/') }}" + "/" + subjectId;

        $('#selectedStudents').empty();
        
        //Cleaning the student table
        $('#studentTableBody').html("");
        
        $.post( url, { _token: token })
        .done(function( data ){
            //sets the given JSON courses at the course select box
            $.each(data.students, function(i, student) {
                
                //Adding the students on the hidden select
                $('#selectedStudents').append(
                    $('<option></option').val(student['student'].id)
                );
                
                //Adding the students on the table
                 $('#studentTableBody').append(
                    "<tr> <td>" + student['student'].name + " " + student['student'].surname + "</td> <td>" + student['student'].ra + "</td> <td>" + student['student'].email + "</td> <td>" + student['priority'] + "</td> </tr>"
                );
            });
        });
    }
</script>

<!-- Display Messages -->
@include('common.messages')

<div class="row">
    <div class="col-lg-10">
        <p>Use os campos abaixo para criar uma turma de DP.</p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!-- Form -->
        <form action="{{ url('/dashboard/dpclass/') }}" method="POST" class="form-horizontal" id="classDPForm">
            {{ csrf_field() }}
            <!-- COURSE -->
            <div class="form-group">
                <label for="course" class="col-lg-2 control-label">
                    Curso
                </label>
                <div class="col-lg-6">
                    <select name="course" class="form-control" id="course" value="{!! old('course') !!}">
                        @foreach ($courses as $course)
                            <option value="{{ $course->id }}">{{ $course->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <!-- SUBJECT -->
            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                <label for="subject" class="col-lg-2 control-label">
                    Disciplina
                </label>
                <div class="col-lg-6">
                    <select name="subject" class="form-control" id="subject" value="{!! old('subject') !!}">
                    </select>
                </div>
            </div>
            
            <!-- PROFESSOR NAME -->
            <div class="form-group {{ $errors->has('professorName') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    Nome do professor
                </label>
                <div class="col-lg-6">
                    <input type="text" name="professorName" class="form-control" 
                        value="{!! old('professorName') !!}" placeholder="Nome do professor"/>
                </div>
            </div>
            
            <!-- PROFESSOR EMAIL -->
            <div class="form-group {{ $errors->has('professorEmail') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    E-mail do professor
                </label>
                <div class="col-lg-6">
                    <input type="text" name="professorEmail" class="form-control" 
                        value="{!! old('professorEmail') !!}" placeholder="E-mail do professor"/>
                </div>
            </div>
            
            <!-- Days -->
            <div class="form-group {{ $errors->has('days') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                     Dias
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Selecione os dias da semana que está disciplina será ministrada."></i>
                 </label>

                <div class="col-lg-6">
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Mon">Segunda-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Tue">Terça-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Wed">Quarta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Thu">Quinta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Fri">Sexta-feira</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="days[]" value="Sat">Sábado</label>
                    </div>
                </div>
            </div>
            
            
            <!-- DESCRIPTION -->
            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                <label for="curso" class="col-lg-2 control-label">
                    Descrição
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                        data-placement="top" title="Descreva informações importantes, como por exemplo os horários dos dias que disicplina será ministrada e o número da sala de aula."></i>
                </label>
                <div class="col-lg-6">
                    <textarea name="description" id="description" class="form-control" rows="5"
                         placeholder="Informações importantes" ></textarea>
                </div>
            </div>
            
            <!-- SELECTED STUDENTS -->
            <!-- This select will store the id of the assigned students -->
            <select name="selectedStudents[]" multiple class="form-control" id="selectedStudents" style="display: none;">
            </select>
        </form>
    </div>
</div>

<!-- STUDENTS -->
<div class="row">
    <div class="col-lg-10">
        <h3>Alunos inscritos</h3>
    </div>
</div>

<!-- SEARCH METHOD
<form class="form-horizontal" id="classDPForm">
    <div class="form-group">
        <label for="institution" class="col-lg-3 control-label">Método de buscar do aluno</label>
        <div class="col-lg-6">
            <div class="btn-group" role="group" aria-label="...">
                <button type="button" id="conventionalSearch" class="btn btn-default active">Nome</button>
                <button type="button" id="optimizedSearch" class="btn btn-default">E-mail</button>
                <button type="button" id="optimizedSearch" class="btn btn-default">Registro Acadêmico (RA)</button>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-lg-3 control-label"></label>
        <div class="col-lg-6">
            <input type="text" name="professorName" class="form-control" 
                value="{!! old('studentName') !!}" placeholder="Nome do aluno"/>
        </div>
    </div>
</form>

-->

<!-- RESULT TABLE -->
<div class="table-responsive" id="studentTable">
    <table class="table table-hover">

        <!-- Table Headings -->
        <thead>
            <th>Nome</th>
            <th>Registro Acadêmico (RA)</th>
            <th>E-mail</th>
            <th>Prioridade</th>
        </thead>

        <!-- Table Body -->
        <tbody id="studentTableBody">
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-lg-1 text-left">
        <a class="btn btn-primary" href="{{ url('/dashboard/dpclass/') }}">Voltar</a>
    </div>
    <div class="col-lg-11 text-right">
        <button class="btn btn-success" id="submitButton">Criar</button>
    </div>
</div>
@endsection
