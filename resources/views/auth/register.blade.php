@extends('layouts.general')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cadastro
        </h1>
    </div>
</div>

<!-- Display Validation Errors -->
@include('common.errors')

<div class="row">
    <div class="col-lg-12">
        @if(isset($institutions) && count($institutions) > 0)
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                
                <!-- INSTITUTION -->
                <div class="form-group{{ $errors->has('institution') ? ' has-error' : '' }}">
                    <label class="col-lg-2 control-label">Instituição</label>
    
                    <div class="col-lg-4">
                        <select name="institution" class="form-control" id="institution">
                            @foreach ($institutions as $institution)
                              <option value="{{ $institution->id }}">{{ $institution->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
    
                <!-- NAME -->
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-lg-2 control-label">Nome</label>
    
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </div>
                
                <!-- SURNAME -->
                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                    <label class="col-lg-2 control-label">Sobrenome</label>
    
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="surname" value="{{ old('surname') }}">
                    </div>
                </div>
    
                <!-- E-mail -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-lg-2 control-label">E-Mail</label>
    
                    <div class="col-lg-4">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>
                
                <!-- RA -->
                <div class="form-group{{ $errors->has('ra') ? ' has-error' : '' }}">
                    <label class="col-lg-2 control-label">Registro Acadêmico (RA)</label>
    
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="ra" value="{{ old('ra') }}">
                    </div>
                </div>
                
                <!-- Password -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-lg-2 control-label">Senha</label>
    
                    <div class="col-lg-4">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>
    
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label class="col-lg-2 control-label">Confirmação de senha</label>
    
                    <div class="col-lg-4">
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>
    
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i>Cadastrar
                        </button>
                    </div>
                </div>
            </form>
        @else
            <div class="alert alert-danger">
                <strong>Cadastro indisponível no momento. Por favor, tente novamente mais tarde.</strong>
            </div>
        @endif
    </div>
</div>
@endsection
