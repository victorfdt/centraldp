@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="container">
        <!-- Current Tasks -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Current Tasks</h3>
            </div>
        </div>
        
        <!-- Display Validation Errors -->
        @include('common.errors')
        
        <!-- Search  -->
        <div class="row">
            <div class="col-lg-12">
                <form class="form-inline" action="{{ url('/task/search') }}" method="POST">
                    {{ csrf_field() }}
                    
                  <div class="form-group has-feedback">
                    <input type="text" name="search" id="search" placeholder="Search" class="form-control">
                    <span aria-hidden="true" class="glyphicon glyphicon-search form-control-feedback"></span>
                  </div>
                  <button type="submit" class="btn btn-success">Search</button>
                </form>
            </div>
        </div>
        
        
        @if (count($tasks) > 0)
        <!-- Table with database results -->
        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
                <th>Task</th>
                <th>&nbsp;</th>
            </thead>

            <!-- Table Body -->
            <tbody>
                @foreach ($tasks as $task)
                    <tr>
                        <!-- Task Name -->
                        <td class="table-text">
                            <div>{{ $task->name }}</div>
                        </td>
                        
                        <!-- Task Description -->
                        <td class="table-text">
                            <div>{{ $task->description }}</div>
                        </td>
                        
                        <!-- Update Button -->
                        <td>
                            <form action="{{ url('task/'.$task->id . '/edit') }}" method="GET">
                                {{ csrf_field() }}
                                {{ method_field('') }}
                    
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </form>
                        </td>
                    
                        <!-- Delete Button -->
                        <td>
                            <form action="{{ url('task/'.$task->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                    
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    
        <div class="row">
            <div class="col-lg-2">
                <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                <a class="btn btn-default" href="{{ url('/task/create') }}" role="button">Add task</a>
            </div>
        </div>
    </div>

    <!-- TODO: Current Tasks -->
@endsection