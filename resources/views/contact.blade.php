@extends('layouts.general')

@section('content')
<!-- Marketing Icons Section -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Contato
            </h1>
        </div>
    </div>
    
    <div class="row">
        <!-- Form Column -->
        <div class="col-md-6">
           <h3>Escreva uma mensagem</h3>
           
            <!-- Display Validation Errors -->
            @include('common.errors')
    
            <!-- New Task Form -->
            <form action="{{ url('/contact_message') }}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
    
                <!-- Name -->
                <div class="form-group">
                    <label for="task" class="col-md-3 control-label">Nome</label>
    
                    <div class="col-md-9">
                        <input type="text" name="name" id="message-name" class="form-control">
                    </div>
                </div>
                
                <!-- Subject -->
                <div class="form-group">
                    <label for="subject" class="col-md-3 control-label">Assunto</label>
    
                    <div class="col-md-9">
                        <input type="text" name="subject" id="subject" class="form-control">
                    </div>
                </div>
                
                <!-- E-mail -->
                <div class="form-group">
                    <label for="email" class="col-md-3 control-label">E-mail</label>
    
                    <div class="col-md-9">
                        <input type="text" name="email" id="email" class="form-control">
                    </div>
                </div>
                
                <!-- Message -->
                <div class="form-group">
                    <label for="message" class="col-md-3 control-label">Mensagem</label>
    
                    <div class="col-md-9">
                        <textarea rows="3" name="message" id="message" class="form-control"></textarea>
                    </div>
                </div>
            </form>
        </div>
        
        <!-- Contact Details Column -->
        <div class="col-md-4 col-md-offset-2">
            <h3>Dados para contato</h3>
            <p>
                Victor Forato Di Trani
            </p>
            <p><i class="fa fa-phone"></i> 
                <abbr title="Phone">Cel.</abbr>: (19) 999-9999</p>
            <p><i class="fa fa-envelope-o"></i> 
                <abbr title="Email">Email</abbr>: <a href="mailto:name@example.com">name@example.com</a>
            </p>
            <p><i class="fa fa-clock-o"></i> 
                <abbr title="Hours">H</abbr>: Monday - Friday: 9:00 AM to 5:00 PM</p>
            <ul class="list-unstyled list-inline list-social-icons">
                <li>
                    <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
                </li>
            </ul>
        </div>
    </div>
   
@endsection