<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    
    'password' => 'As senhas devem conter no mínimo 6 caracteres e devem ser iguais.',
    'reset' => 'Sua senha foi renovada!',
    'sent' => 'Um email com o link da renovação de senha foi enviado.',
    'token' => 'Esse token para renovação de senha é inválido.',
    'user' => "Não foi encontrado nenhum usuário com o email informado.",

];
