<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'As informações fornecidas não foram encontradas no sistema.',
    'throttle' => 'Muitas tentativas de acesso foram feitas. Por favor, tente novamente em :seconds segundos.',

];
