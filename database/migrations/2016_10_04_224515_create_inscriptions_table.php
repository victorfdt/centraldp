<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscriptions', function(Blueprint $table){
            $table->increments('id');
            $table->integer('status');
            $table->string('days', 50);
            $table->integer('priority');
           
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')
                    ->on('users')->onDelete('cascade');
                    
            $table->integer('subject_id')->unsigned()->index();
            $table->foreign('subject_id')->references('id')
                    ->on('subjects')->onDelete('cascade');
            
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inscriptions');
    }
}
