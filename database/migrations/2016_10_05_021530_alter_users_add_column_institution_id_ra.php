<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAddColumnInstitutionIdRa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('ra', 20);
            $table->integer('institution_id')->nullable()->unsigned();
            $table->foreign('institution_id')->references('id')
                    ->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropForeign('users_institution_id_foreign');
            $table->dropColumn('institution_id');
            $table->dropColumn('ra');
        });
    }
}
