<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDpclassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Table of the DP Classes
      Schema::create('dpclasses', function(Blueprint $table){
         $table->increments('id');
         $table->integer('status');
         $table->date('start_date');
         $table->date('end_date');
         $table->string('professor_name', 100);
         $table->string('professor_email', 100);
         $table->text('description');
         $table->string('days', 50);

         $table->integer('subject_id')->unsigned()->index();
         $table->foreign('subject_id')->references('id')
                 ->on('subjects')->onDelete('cascade');

         $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dpclasses');
    }
}
