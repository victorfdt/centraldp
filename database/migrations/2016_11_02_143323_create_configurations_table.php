<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //Table of the configuration properties
      Schema::create('configurations', function(Blueprint $table){
         $table->integer('max_num_ins');
         $table->timestamps();
      });

      //Adding a default value
      DB::table('configurations')->insert([
        ['max_num_ins' => '5'],
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configurations');
    }
}
