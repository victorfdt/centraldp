<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::table('users')->insert([
        ['name' => 'Master',
         'surname' => 'CentralDP',
         'email' => 'master@master.com',
         'type' => '3',
         'institution_id' => null,
         'password' => bcrypt('master123')],
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->where('email', '=', 'master@master.com')->delete();
    }
}
